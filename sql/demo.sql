/*
 Navicat Premium Data Transfer

 Source Server         : 本地连接
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : 127.0.0.1:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 17/07/2023 16:43:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for A
-- ----------------------------
DROP TABLE IF EXISTS `A`;
CREATE TABLE `A` (
  `F1` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `F2` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `F3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of A
-- ----------------------------
BEGIN;
INSERT INTO `A` (`F1`, `F2`, `F3`) VALUES ('1', '2', '一般');
INSERT INTO `A` (`F1`, `F2`, `F3`) VALUES ('1', '', '特大');
INSERT INTO `A` (`F1`, `F2`, `F3`) VALUES ('1', '4', '重大');
INSERT INTO `A` (`F1`, `F2`, `F3`) VALUES ('1', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for b
-- ----------------------------
DROP TABLE IF EXISTS `b`;
CREATE TABLE `b` (
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `quantity` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of b
-- ----------------------------
BEGIN;
INSERT INTO `b` (`date`, `quantity`) VALUES ('1', 1000);
INSERT INTO `b` (`date`, `quantity`) VALUES ('6', 2000);
INSERT INTO `b` (`date`, `quantity`) VALUES ('12', 3000);
COMMIT;

-- ----------------------------
-- Table structure for Data
-- ----------------------------
DROP TABLE IF EXISTS `Data`;
CREATE TABLE `Data` (
  `createDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of Data
-- ----------------------------
BEGIN;
INSERT INTO `Data` (`createDate`) VALUES ('2023-03-20');
INSERT INTO `Data` (`createDate`) VALUES ('2023-03-21');
COMMIT;

-- ----------------------------
-- Table structure for ecs_sy_charts
-- ----------------------------
DROP TABLE IF EXISTS `ecs_sy_charts`;
CREATE TABLE `ecs_sy_charts` (
  `map` int DEFAULT NULL,
  `lines` int DEFAULT NULL,
  `bar` int DEFAULT NULL,
  `line` int DEFAULT NULL,
  `pie` int DEFAULT NULL,
  `scatter` int DEFAULT NULL,
  `candlestick` int DEFAULT NULL,
  `radar` int DEFAULT NULL,
  `heatmap` int DEFAULT NULL,
  `treemap` int DEFAULT NULL,
  `graph` int DEFAULT NULL,
  `boxplot` int DEFAULT NULL,
  `parallel` int DEFAULT NULL,
  `gauge` int DEFAULT NULL,
  `funnel` int DEFAULT NULL,
  `sankey` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of ecs_sy_charts
-- ----------------------------
BEGIN;
INSERT INTO `ecs_sy_charts` (`map`, `lines`, `bar`, `line`, `pie`, `scatter`, `candlestick`, `radar`, `heatmap`, `treemap`, `graph`, `boxplot`, `parallel`, `gauge`, `funnel`, `sankey`) VALUES (3238, 2164, 7561, 7778, 7355, 2405, 1842, 2090, 1762, 1593, 2060, 1537, 1908, 2107, 1692, 1568);
COMMIT;

-- ----------------------------
-- Table structure for ecs_sy_components
-- ----------------------------
DROP TABLE IF EXISTS `ecs_sy_components`;
CREATE TABLE `ecs_sy_components` (
  `geo` int DEFAULT NULL,
  `title` int DEFAULT NULL,
  `legend` int DEFAULT NULL,
  `tooltip` int DEFAULT NULL,
  `grid` int DEFAULT NULL,
  `markPoint` int DEFAULT NULL,
  `markLine` int DEFAULT NULL,
  `timeline` int DEFAULT NULL,
  `dataZoom` int DEFAULT NULL,
  `visualMap` int DEFAULT NULL,
  `toolbox` int DEFAULT NULL,
  `polar` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of ecs_sy_components
-- ----------------------------
BEGIN;
INSERT INTO `ecs_sy_components` (`geo`, `title`, `legend`, `tooltip`, `grid`, `markPoint`, `markLine`, `timeline`, `dataZoom`, `visualMap`, `toolbox`, `polar`) VALUES (2788, 9575, 9400, 9466, 9266, 3419, 2984, 2739, 2744, 2788, 2466, 1945);
COMMIT;

-- ----------------------------
-- Table structure for ecs_sy_downloadJson
-- ----------------------------
DROP TABLE IF EXISTS `ecs_sy_downloadJson`;
CREATE TABLE `ecs_sy_downloadJson` (
  `echarts:min:js` int DEFAULT NULL,
  `echarts:simple:min:js` int DEFAULT NULL,
  `echarts:common:min:js` int DEFAULT NULL,
  `echarts:js` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of ecs_sy_downloadJson
-- ----------------------------
BEGIN;
INSERT INTO `ecs_sy_downloadJson` (`echarts:min:js`, `echarts:simple:min:js`, `echarts:common:min:js`, `echarts:js`) VALUES (17365, 4079, 6929, 14890);
COMMIT;

-- ----------------------------
-- Table structure for ecs_sy_themeJson
-- ----------------------------
DROP TABLE IF EXISTS `ecs_sy_themeJson`;
CREATE TABLE `ecs_sy_themeJson` (
  `dark:js` int DEFAULT NULL,
  `infographic:js` int DEFAULT NULL,
  `shine:js` int DEFAULT NULL,
  `roma:js` int DEFAULT NULL,
  `macarons:js` int DEFAULT NULL,
  `vintage:js` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of ecs_sy_themeJson
-- ----------------------------
BEGIN;
INSERT INTO `ecs_sy_themeJson` (`dark:js`, `infographic:js`, `shine:js`, `roma:js`, `macarons:js`, `vintage:js`) VALUES (1594, 925, 1608, 721, 2179, 1982);
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int DEFAULT NULL COMMENT '父ID',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '菜单名称',
  `order_num` int DEFAULT NULL COMMENT '排列序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (1, 0, '工作台', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (2, 1, '系统管理', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (3, 2, '部门/角色管理', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (4, 3, '配置部门/角色', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (5, 1, '账号管理', 2);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (6, 5, '修改账号', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (7, 5, '修改密码', 2);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (8, 1, '报表管理', 3);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (9, 8, '报表权限设置', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (10, 8, '新报表权限运行', 2);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (11, 1, '产品管理', 4);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (12, 11, '添加产品', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (13, 12, '批量添加产品', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (14, 13, '批量导出', 1);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (15, 13, '批量导入', 2);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (16, 11, '删除产品', 2);
INSERT INTO `menu` (`id`, `parent_id`, `menu_name`, `order_num`) VALUES (17, 16, '批量删除产品', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_login_time
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_time`;
CREATE TABLE `sys_login_time` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Records of sys_login_time
-- ----------------------------
BEGIN;
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (1, '123', '2023-02-13 10:21:40', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (2, '123', '2023-02-13 10:39:14', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (3, 'admin', '2023-02-13 10:50:49', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (4, 'admin', '2023-02-13 11:10:23', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (5, 'admin', '2023-02-13 14:18:52', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (6, 'admin', '2023-02-13 14:41:50', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (7, 'admin', '2023-02-13 14:42:07', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (8, 'admin', '2023-02-13 14:42:32', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (9, 'admin', '2023-02-13 14:43:16', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (10, 'admin', '2023-02-13 15:08:48', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (11, 'admin', '2023-02-13 15:09:16', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (12, 'admin', '2023-02-13 16:00:57', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (13, 'admin', '2023-02-13 17:05:09', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (14, 'admin', '2023-02-14 09:58:18', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (15, 'admin', '2023-02-14 13:40:59', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (16, 'admin', '2023-02-14 14:41:31', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (17, 'admin', '2023-02-14 15:55:02', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (18, 'admin', '2023-02-14 15:59:50', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (19, 'admin', '2023-02-14 17:05:03', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (20, 'admin', '2023-02-15 08:54:05', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (21, 'admin', '2023-02-15 10:02:25', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (22, 'admin', '2023-02-15 11:05:58', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (23, 'admin', '2023-02-15 13:19:02', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (24, 'admin', '2023-02-15 14:26:25', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (25, 'admin', '2023-02-15 15:46:58', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (26, 'admin', '2023-02-16 08:36:00', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (27, 'admin', '2023-02-16 10:02:25', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (28, 'admin', '2023-02-16 11:03:43', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (29, 'admin', '2023-02-16 13:21:15', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (30, 'admin', '2023-02-16 14:21:45', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (31, 'admin', '2023-02-16 15:13:58', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (32, 'admin', '2023-02-16 15:16:01', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (33, 'admin', '2023-02-16 15:16:08', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (34, 'admin', '2023-02-16 15:17:43', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (35, 'admin', '2023-02-16 15:18:39', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (36, 'admin', '2023-02-16 15:20:43', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (37, 'hugaoqiang', '2023-02-16 15:24:35', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (38, 'admin', '2023-02-16 15:26:31', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (39, 'admin', '2023-02-16 15:28:17', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (40, 'admin', '2023-02-16 15:28:43', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (41, 'admin', '2023-02-16 15:32:27', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (42, 'admin', '2023-02-16 15:56:35', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (43, 'admin', '2023-02-16 15:57:51', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (44, 'admin', '2023-02-16 15:59:59', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (45, 'admin', '2023-02-16 16:00:29', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (46, 'admin', '2023-02-16 16:00:34', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (47, 'admin', '2023-02-16 16:01:46', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (48, 'admin', '2023-02-16 16:03:00', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (49, 'admin', '2023-02-16 16:15:07', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (50, 'admin', '2023-02-16 16:31:01', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (51, 'admin', '2023-02-16 16:31:27', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (52, 'admin', '2023-02-16 16:38:07', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (53, 'admin', '2023-02-16 16:52:08', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (54, 'admin', '2023-02-17 13:20:57', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (55, 'admin', '2023-02-17 13:38:17', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (56, 'admin', '2023-02-17 13:46:00', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (57, 'admin', '2023-02-17 13:55:29', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (58, 'admin', '2023-02-17 13:57:53', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (59, 'admin', '2023-02-17 14:15:21', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (60, 'admin', '2023-02-17 14:52:57', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (61, 'admin', '2023-02-20 10:50:29', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (62, 'lisi', '2023-02-20 13:25:34', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (63, 'admin', '2023-02-20 14:04:36', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (64, 'admin', '2023-02-21 14:50:07', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (65, 'admin', '2023-02-22 09:05:38', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (66, 'admin', '2023-02-22 09:08:26', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (67, 'admin', '2023-02-22 09:09:25', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (68, 'admin', '2023-02-22 09:23:58', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (69, 'admin', '2023-02-22 15:10:16', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (70, 'lisi', '2023-02-23 09:47:52', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (71, 'hugaoqiang', '2023-02-23 09:50:03', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (72, 'lisi', '2023-02-23 10:34:33', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (73, 'admin', '2023-02-23 14:13:20', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (74, 'admin', '2023-02-24 09:12:45', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (75, 'admin', '2023-02-24 10:50:11', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (76, 'admin', '2023-02-24 17:18:05', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (77, 'admin', '2023-02-28 09:17:01', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (78, 'lisi', '2023-02-28 13:58:33', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (79, 'admin', '2023-02-28 13:59:19', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (80, 'lisi', '2023-02-28 14:02:37', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (81, 'hugaoqiang', '2023-02-28 14:06:59', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (82, 'hugaoqiang', '2023-02-28 14:15:26', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (83, 'admin', '2023-02-28 14:22:41', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (84, 'admin', '2023-02-28 16:14:01', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (85, 'admin', '2023-02-28 16:16:06', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (86, 'admin', '2023-02-28 16:19:29', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (87, 'admin', '2023-02-28 16:59:36', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (88, 'admin', '2023-03-03 13:28:00', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (89, 'admin', '2023-03-06 16:11:44', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (90, 'admin', '2023-03-08 13:57:22', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (91, 'admin', '2023-03-09 09:36:55', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (92, 'admin', '2023-03-09 15:56:36', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (93, 'hugaoqiang', '2023-03-10 15:06:28', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (94, 'admin', '2023-03-10 16:01:03', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (95, 'admin', '2023-03-10 16:04:45', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (96, 'admin', '2023-03-13 16:11:56', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (97, 'admin', '2023-03-17 11:07:29', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (98, 'zhangsan', '2023-07-12 16:33:45', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (99, 'admin', '2023-07-12 16:58:09', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (100, 'admin', '2023-07-13 09:08:53', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (101, 'admin', '2023-07-13 09:10:46', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (102, 'admin', '2023-07-13 09:14:45', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (103, 'admin', '2023-07-13 09:45:37', '0');
INSERT INTO `sys_login_time` (`id`, `name`, `time`, `status`) VALUES (104, 'admin', '2023-07-13 13:03:01', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组建路径',
  `visaible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0正常  1.停用）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0正常  1.停用）',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标志',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` int DEFAULT '0' COMMENT '是否删除（0.未删除  1.已删除）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (1, '部门', 'dept', 'system/dept/index', '0', '0', 'system:dept:list', '#', '管理员', '2023-02-16 09:17:43', '管理员', '2023-02-16 09:17:43', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (2, '测试', 'test', 'system/test/index', '0', '0', 'system:test:list', '#', '管理员', '2023-02-16 09:17:43', '管理员', '2023-02-16 09:17:43', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (3, '开发', 'dev', 'system/dev/index', '0', '1', 'system:dev:list', '#', '管理员', '2023-02-16 09:17:43', '管理员', '2023-02-16 09:17:43', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (4, '生产', 'crm', 'system/crm/index', '0', '0', 'system:crm:list', '#', '管理员', '2023-02-16 09:26:38', '管理员', '2023-02-16 09:26:38', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (5, '其他', 'qts', 'system/qts/index', '0', '0', 'system:qts:list', '#', '管理员', '2023-02-16 16:54:29', '管理员', '2023-02-16 16:54:29', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (6, '权限', 'qxs', 'system/qxs/index', '0', '0', 'system:qxs:list', '#', '管理员', '2023-02-17 14:37:54', '管理员', '2023-02-17 14:37:54', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (7, '优德', 'qwe', 'system/qwe/index', '0', '0', 'system:qwe:list', '#', '管理员', '2023-02-17 14:39:21', '管理员', '2023-02-17 14:39:21', 0, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (8, '1233', '123', 'system/123/index', '0', '0', 'system:123:list', '#', '管理员', '2023-02-17 14:50:26', '管理员', '2023-02-17 14:50:26', 1, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (9, '权限3', 'qxs2', 'system/qxs2/index', '0', '1', 'system:qxs2:list', '#', '管理员', '2023-02-17 17:01:00', '管理员', '2023-02-17 17:01:00', 1, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (10, '123', '123', 'system/123/index', '0', '0', 'system:123:list', '#', '管理员', '2023-02-20 13:17:18', '管理员', '2023-02-20 13:17:18', 1, NULL);
INSERT INTO `sys_menu` (`id`, `menu_name`, `path`, `component`, `visaible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `remark`) VALUES (11, '人工', 'api', 'system/api/index', '0', '0', 'system:api:list', '#', '管理员', '2023-02-28 09:42:20', '管理员', '2023-02-28 09:42:20', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_prd
-- ----------------------------
DROP TABLE IF EXISTS `sys_prd`;
CREATE TABLE `sys_prd` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `prd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '产品名称',
  `prd_dm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '产品代码',
  `net` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '净值',
  `fbalance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '份额',
  `favalable` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '金额',
  `ins_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '机构名称',
  `ins_dm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '机构代码',
  `new_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日期',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int NOT NULL DEFAULT '0' COMMENT '默认0  未删除0  已删除1',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_prd
-- ----------------------------
BEGIN;
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (1, '产品test1', '1', '0.8812', '123.1', '123.1', '机构test1', '001', '2023-03-09', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (2, '产品test2', '1', '0.8812', '123.1', '123.1', '机构test2', '001', '2023-03-01', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (3, '产品test3', '1', '0.8812', '123.1', '123.1', '机构test3', '001', '2023-03-02', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (4, '产品test4', '1', '0.8812', '123.1', '123.1', '机构test4', '001', '2023-03-07', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (5, '产品test5', '1', '0.8812', '123.1', '123.1', '机构test5', '001', '2023-02-07', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (6, '产品test6', '1', '0.8812', '123.1', '123.1', '机构test6', '001', '2023-02-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (7, '产品test7', '1', '0.8812', '123.1', '123.1', '机构test7', '001', '2022-012-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (8, '产品test8', '1', '0.8812', '123.1', '123.1', '机构test8', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (9, '产品test9', '1', '0.8812', '123.1', '123.1', '机构test9', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (10, '产品test10', '1', '0.8812', '123.1', '123.1', '机构test10', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (11, '产品test11', '1', '0.8812', '123.1', '123.1', '机构test11', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (12, '产品test12', '1', '0.8812', '123.1', '123.1', '机构test12', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (13, '产品test13', '1', '0.8812', '123.1', '123.1', '机构test13', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (14, '产品test14', '1', '0.8812', '123.1', '123.1', '机构test14', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (15, '产品test15', '1', '0.8812', '123.1', '123.1', '机构test15', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (16, '产品test16', '1', '0.8812', '123.1', '123.1', '机构test16', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (17, '产品test17', '1', '0.8812', '123.1', '123.1', '机构test17', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (18, '产品test18', '1', '0.8812', '123.1', '123.1', '机构test18', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (19, '产品test19', '1', '0.8812', '123.1', '123.1', '机构test19', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (20, '产品test20', '1', '0.8812', '123.1', '123.1', '机构test20', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (21, '产品test21', '1', '0.8812', '123.1', '123.1', '机构test21', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (22, '产品test22', '1', '0.8812', '123.1', '123.1', '机构test22', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (23, '产品test23', '1', '0.8812', '123.1', '123.1', '机构test23', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (24, '产品test24', '1', '0.8812', '123.1', '123.1', '机构test24', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (25, '产品test25', '1', '0.8812', '123.1', '123.1', '机构test25', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (26, '产品test26', '1', '0.8812', '123.1', '123.1', '机构test26', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (27, '产品test27', '1', '0.8812', '123.1', '123.1', '机构test27', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:31');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (28, '产品test28', '1', '0.8812', '123.1', '123.1', '机构test28', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (29, '产品test29', '1', '0.8812', '123.1', '123.1', '机构test29', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (30, '产品test30', '1', '0.8812', '123.1', '123.1', '机构test30', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (31, '产品test31', '1', '0.8812', '123.1', '123.1', '机构test31', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (32, '产品test32', '1', '0.8812', '123.1', '123.1', '机构test32', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (33, '产品test33', '1', '0.8812', '123.1', '123.1', '机构test33', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (34, '产品test34', '1', '0.8812', '123.1', '123.1', '机构test34', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (35, '产品test35', '1', '0.8812', '123.1', '123.1', '机构test35', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (36, '产品test36', '1', '0.8812', '123.1', '123.1', '机构test36', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (37, '产品test37', '1', '0.8812', '123.1', '123.1', '机构test37', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (38, '产品test38', '1', '0.8812', '123.1', '123.1', '机构test38', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (39, '产品test39', '1', '0.8812', '123.1', '123.1', '机构test39', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (40, '产品test40', '1', '0.8812', '123.1', '123.1', '机构test40', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (41, '产品test41', '1', '0.8812', '123.1', '123.1', '机构test41', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (42, '产品test42', '1', '0.8812', '123.1', '123.1', '机构test42', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (43, '产品test43', '1', '0.8812', '123.1', '123.1', '机构test43', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (44, '产品test44', '1', '0.8812', '123.1', '123.1', '机构test44', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (45, '产品test45', '1', '0.8812', '123.1', '123.1', '机构test45', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (46, '产品test46', '1', '0.8812', '123.1', '123.1', '机构test46', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (47, '产品test47', '1', '0.8812', '123.1', '123.1', '机构test47', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (48, '产品test48', '1', '0.8812', '123.1', '123.1', '机构test48', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (49, '产品test49', '1', '0.8812', '123.1', '123.1', '机构test49', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (50, '产品test50', '1', '0.8812', '123.1', '123.1', '机构test50', '001', '2022-04-01', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (51, '产品test51', '1', '0.8812', '123.1', '123.1', '机构test51', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (52, '产品test52', '1', '0.8812', '123.1', '123.1', '机构test52', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (53, '产品test53', '1', '0.8812', '123.1', '123.1', '机构test53', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (54, '产品test54', '1', '0.8812', '123.1', '123.1', '机构test54', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (55, '产品test55', '1', '0.8812', '123.1', '123.1', '机构test55', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (56, '产品test56', '1', '0.8812', '123.1', '123.1', '机构test56', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (57, '产品test57', '1', '0.8812', '123.1', '123.1', '机构test57', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (58, '产品test58', '1', '0.8812', '123.1', '123.1', '机构test58', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (59, '产品test59', '1', '0.8812', '123.1', '123.1', '机构test59', '001', '2023-03-09', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (60, '产品test60', '1', '0.8812', '123.1', '123.1', '机构test60', '001', '2023-03-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (61, '产品test61', '1', '0.8812', '123.1', '123.1', '机构test61', '001', '2023-03-09', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (62, '产品test62', '1', '0.8812', '123.1', '123.1', '机构test62', '001', '2023-03-01', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (63, '产品test63', '1', '0.8812', '123.1', '123.1', '机构test63', '001', '2023-03-02', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (64, '产品test64', '1', '0.8812', '123.1', '123.1', '机构test64', '001', '2023-03-07', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (65, '产品test65', '1', '0.8812', '123.1', '123.1', '机构test65', '001', '2023-02-07', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (66, '产品test66', '1', '0.8812', '123.1', '123.1', '机构test66', '001', '2023-02-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (67, '产品test67', '1', '0.8812', '123.1', '123.1', '机构test67', '001', '2022-012-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (68, '产品test68', '1', '0.8812', '123.1', '123.1', '机构test68', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (69, '产品test69', '1', '0.8812', '123.1', '123.1', '机构test69', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (70, '产品test70', '1', '0.8812', '123.1', '123.1', '机构test70', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (71, '产品test71', '1', '0.8812', '123.1', '123.1', '机构test71', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (72, '产品test72', '1', '0.8812', '123.1', '123.1', '机构test72', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (73, '产品test73', '1', '0.8812', '123.1', '123.1', '机构test73', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (74, '产品test74', '1', '0.8812', '123.1', '123.1', '机构test74', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (75, '产品test75', '1', '0.8812', '123.1', '123.1', '机构test75', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (76, '产品test76', '1', '0.8812', '123.1', '123.1', '机构test76', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (77, '产品test77', '1', '0.8812', '123.1', '123.1', '机构test77', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (78, '产品test78', '1', '0.8812', '123.1', '123.1', '机构test78', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (79, '产品test79', '1', '0.8812', '123.1', '123.1', '机构test79', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (80, '产品test80', '1', '0.8812', '123.1', '123.1', '机构test80', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (81, '产品test81', '1', '0.8812', '123.1', '123.1', '机构test81', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (82, '产品test82', '1', '0.8812', '123.1', '123.1', '机构test82', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (83, '产品test83', '1', '0.8812', '123.1', '123.1', '机构test83', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (84, '产品test84', '1', '0.8812', '123.1', '123.1', '机构test84', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (85, '产品test85', '1', '0.8812', '123.1', '123.1', '机构test85', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (86, '产品test86', '1', '0.8812', '123.1', '123.1', '机构test86', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (87, '产品test87', '1', '0.8812', '123.1', '123.1', '机构test87', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (88, '产品test88', '1', '0.8812', '123.1', '123.1', '机构test88', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (89, '产品test89', '1', '0.8812', '123.1', '123.1', '机构test89', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (90, '产品test90', '1', '0.8812', '123.1', '123.1', '机构test90', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (91, '产品test91', '1', '0.8812', '123.1', '123.1', '机构test91', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (92, '产品test92', '1', '0.8812', '123.1', '123.1', '机构test92', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (93, '产品test93', '1', '0.8812', '123.1', '123.1', '机构test93', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (94, '产品test94', '1', '0.8812', '123.1', '123.1', '机构test94', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (95, '产品test95', '1', '0.8812', '123.1', '123.1', '机构test95', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (96, '产品test96', '1', '0.8812', '123.1', '123.1', '机构test96', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (97, '产品test97', '1', '0.8812', '123.1', '123.1', '机构test97', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (98, '产品test98', '1', '0.8812', '123.1', '123.1', '机构test98', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (99, '产品test99', '1', '0.8812', '123.1', '123.1', '机构test99', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (100, '产品test100', '1', '0.8812', '123.1', '123.1', '机构test100', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (101, '产品test101', '1', '0.8812', '123.1', '123.1', '机构test101', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (102, '产品test102', '1', '0.8812', '123.1', '123.1', '机构test102', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (103, '产品test103', '1', '0.8812', '123.1', '123.1', '机构test103', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (104, '产品test104', '1', '0.8812', '123.1', '123.1', '机构test104', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (105, '产品test105', '1', '0.8812', '123.1', '123.1', '机构test105', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (106, '产品test106', '1', '0.8812', '123.1', '123.1', '机构test106', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (107, '产品test107', '1', '0.8812', '123.1', '123.1', '机构test107', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (108, '产品test108', '1', '0.8812', '123.1', '123.1', '机构test108', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (109, '产品test109', '1', '0.8812', '123.1', '123.1', '机构test109', '001', '2022-04-08', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (110, '产品test110', '1', '0.8812', '123.1', '123.1', '机构test110', '001', '2022-04-01', '管理员', '2022-04-08 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (111, '产品test111', '1', '0.8812', '123.1', '123.1', '机构test111', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (112, '产品test112', '1', '0.8812', '123.1', '123.1', '机构test112', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (113, '产品test113', '1', '0.8812', '123.1', '123.1', '机构test113', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (114, '产品test114', '1', '0.8812', '123.1', '123.1', '机构test114', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (115, '产品test115', '1', '0.8812', '123.1', '123.1', '机构test115', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (116, '产品test116', '1', '0.8812', '123.1', '123.1', '机构test116', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (117, '产品test117', '1', '0.8812', '123.1', '123.1', '机构test117', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (118, '产品test118', '1', '0.8812', '123.1', '123.1', '机构test118', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (119, '产品test119', '1', '0.8812', '123.1', '123.1', '机构test119', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-14 10:12:32');
INSERT INTO `sys_prd` (`ID`, `prd_name`, `prd_dm`, `net`, `fbalance`, `favalable`, `ins_name`, `ins_dm`, `new_date`, `create_by`, `create_time`, `del_flag`, `update_by`, `update_time`) VALUES (120, '产品test120', '1', '0.8812', '123.1', '123.1', '机构test120', '001', '2022-05-10', '管理员', '2022-05-10 00:00:00', 0, '管理员', '2023-03-09 09:48:02');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `role_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色权限字符串',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '角色状态（0正常  1停用）',
  `del_flag` int DEFAULT '0',
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, 'Admin', 'admin', '0', 0, '管理员', '2023-02-27 09:42:52', '管理员', '2023-02-27 09:42:55', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, 'Coder', 'coder', '0', 0, '管理员', '2023-02-28 09:06:56', '管理员', '2023-02-28 09:07:10', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, 'CEO', 'ceo', '0', 0, '管理员', '2023-02-28 09:07:00', '管理员', '2023-02-28 09:07:14', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, 'QWE', 'qwe', '0', 0, '管理员', '2023-02-21 15:13:31', '管理员', '2023-02-21 15:13:31', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (5, 'ASD', 'asd', '0', 0, '管理员', '2023-02-22 16:19:47', '管理员', '2023-02-22 16:19:47', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (6, 'ZXC', 'zxc', '0', 0, '管理员', '2023-02-24 10:58:16', '管理员', '2023-02-24 10:58:16', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (7, 'ROLE_ADMIN', 'admin', '0', 0, '管理员', '2023-05-06 09:14:47', '管理员', '2023-05-06 09:14:50', NULL);
INSERT INTO `sys_role` (`id`, `role_name`, `role_key`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (8, 'ROLE_USER', 'user', '0', 0, '管理员', '2023-05-06 09:14:47', '管理员', '2023-05-06 09:14:47', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint DEFAULT NULL COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 2);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (4, 5);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (5, 7);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (1, 1);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (1, 5);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `number` int NOT NULL DEFAULT '0' COMMENT '登录次数',
  `update_by` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '修改人',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '状态（0正常  1停用）',
  `del_flag` int DEFAULT '0' COMMENT '默认0  未删除0  已删除1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (1, 'admin', '管理员', '$2a$10$8kT/GOXkKCogT2lN1aZeged8IZpHvymdwMEXarB3/tFLQrANDAU0K', '2022-06-16', 'admin', '2022-06-16 00:00:00', 1, '管理员', '0', 0);
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (2, 'lisi', '李四', '$2a$10$i7bJsqPndOGxjD3.tzC4nePORDScTKbAZlVwtzobF6Zc.HijIMLJe', '2022-06-16', 'admin', '2022-06-16 00:00:00', 1, '李四', '0', 0);
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (3, 'zhangsan', '张三', '$2a$10$i7bJsqPndOGxjD3.tzC4nePORDScTKbAZlVwtzobF6Zc.HijIMLJe', '2022-06-16', 'admin', '2022-06-16 10:50:49', 1, 'admin', '0', 0);
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (4, 'libai', '李白', '$2a$10$i7bJsqPndOGxjD3.tzC4nePORDScTKbAZlVwtzobF6Zc.HijIMLJe', '2022-08-22', 'admin', '2022-08-22 11:02:34', 0, 'admin', '0', 0);
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (5, 'eg', '二哥', '$2a$10$i7bJsqPndOGxjD3.tzC4nePORDScTKbAZlVwtzobF6Zc.HijIMLJe', '2022-08-24', 'admin', '2022-08-24 14:13:33', 0, '管理员', '0', 0);
INSERT INTO `sys_user` (`id`, `username`, `nick_name`, `password`, `create_time`, `create_by`, `update_time`, `number`, `update_by`, `status`, `del_flag`) VALUES (6, 'hugaoqiang', '胡高强', '$2a$10$i7bJsqPndOGxjD3.tzC4nePORDScTKbAZlVwtzobF6Zc.HijIMLJe', '2023-02-13', 'admin', '2023-02-13 16:01:43', 1, '管理员', '0', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (3, 2);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (2, 1);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (4, 2);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (5, 2);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (6, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (6, 2);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (8, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (1, 2);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (28, 7);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (29, 8);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
