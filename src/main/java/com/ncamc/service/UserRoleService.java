package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.entity.Role;
import com.ncamc.entity.User;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface UserRoleService extends IService<Role>
{
    /**
     * 查询所有用户角色分页信息
     *
     * @param params
     * @param page
     * @return
     */
    ApiResult findAllUserRolePage(Page<User> page, Map<String, Object> params);

    /**
     * 查询所有角色信息
     *
     * @param id
     * @return
     */
    ApiResult findAllRole(Long id);

    /**
     * 添加用户角色
     *
     * @param params
     * @return
     */
    ApiResult saveUserRole(Map<String, Object> params);

    /**
     * 查询当前用户是否拥有角色
     *
     * @param id
     * @return
     */
    ApiResult selectUserRoleByIdU(Long id);

    /**
     * 修改当前角色信息
     *
     * @param params
     * @return
     */
    ApiResult updateUserRole(Map<String, Object> params);

    /**
     * 删除当前用户角色
     *
     * @param params
     * @return
     */
    ApiResult delUserRole(Map<String, Object> params);
}
