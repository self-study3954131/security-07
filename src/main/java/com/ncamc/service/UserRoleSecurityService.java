package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface UserRoleSecurityService
{
    /**
     * 查询用户角色权限分页信息
     *
     * @param page
     * @param params
     * @return
     */
    ApiResult findAllUserRoleSecurityPage(Page<Menu> page, Map<String, Object> params);
}
