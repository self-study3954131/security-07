package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.entity.Role;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface RoleService extends IService<Role>
{
    /**
     * 查询角色分页信息
     *
     * @param page
     * @param params
     * @return
     */
    ApiResult findAllPageRole(Page<Role> page, Map<String, Object> params);

    /**
     * 新增角色信息
     *
     * @param params
     * @return
     */
    ApiResult saveRole(Map<String, Object> params);

    /**
     * 查询角色是否被使用
     *
     * @param id
     * @return
     */
    ApiResult selectRoleUserById(Long id);

    /**
     * 查询当前角色信息
     *
     * @param id
     * @return
     */
    ApiResult findRoleByID(Long id);

    /**
     * 修改当前权限信息
     *
     * @param params
     * @return
     */
    ApiResult updateRole(Map<String, Object> params);

    /**
     * 删除当前角色信息
     *
     * @param id
     * @return
     */
    ApiResult delRole(Long id);

    /**
     * 停用  恢复
     *
     * @param id
     * @return
     */
    ApiResult deactivate(Long id);
}
