package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.entity.Menu;
import com.ncamc.entity.User;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface RoleSecurityService extends IService<Menu>
{
    /**
     * 查询角色权限分页信息
     *
     * @param params
     * @param page
     * @return
     */
    ApiResult findAllRoleSecurityPage(Page<User> page, Map<String, Object> params);

    /**
     * 查询当前所选用户和权限信息
     *
     * @param id
     * @return
     */
    ApiResult findRoleSecurityByID(Long id);

    /**
     * 新增角色权限信息
     *
     * @param params
     * @return
     */
    ApiResult saveRoleSecurity(Map<String, Object> params);

    /**
     * 查询当前角色权限
     *
     * @param id
     * @return
     */
    ApiResult selectRoleMenuById(Long id);

    /**
     * 修改当前角色权限信息
     *
     * @param params
     * @return
     */
    ApiResult updateRoleSecurity(Map<String, Object> params);

    /**
     * 删除当前角色权限
     *
     * @param params
     * @return
     */
    ApiResult delRoleSecurity(Map<String, Object> params);
}
