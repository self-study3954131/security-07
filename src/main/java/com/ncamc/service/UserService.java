package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.entity.User;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface UserService extends IService<User>
{
    /**
     * 注册用户和新增用户信息
     *
     * @param user
     * @return
     */
    ApiResult register(User user);

    /**
     * 登录
     *
     * @param user
     * @return
     */
    ApiResult login(User user);

    /**
     * 获取用户名称
     *
     * @return
     */
    ApiResult getUsername();

    /**
     * 获取用户最后登录时间
     *
     * @param apiResult
     * @return
     */
    ApiResult getLoginTime(ApiResult apiResult);

    /**
     * 修改密码
     *
     * @param params
     * @return
     */
    ApiResult updatePassword(Map<String, Object> params);

    /**
     * 退出
     *
     * @return
     */
    ApiResult exit();

    /**
     * 查询用户分页信息
     *
     * @param params
     * @return
     */
    ApiResult listPage(Page<User> page, Map<String, Object> params);

    /**
     * 查询当前用户信息
     *
     * @param id
     * @return
     */
    ApiResult selectById(Long id);

    /**
     * 修改当前用户信息
     *
     * @param user
     * @return
     */
    ApiResult updateUser(User user);

    /**
     * 删除当前用户信息
     *
     * @param id
     * @return
     */
    ApiResult delUser(Long id);
}
