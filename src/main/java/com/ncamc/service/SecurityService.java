package com.ncamc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.entity.dto.ApiResult;

import java.util.Map;

public interface SecurityService
{
    /**
     * 查询权限分页信息
     *
     * @param page
     * @param params
     * @return
     */
    ApiResult list(Page<Menu> page, Map<String, Object> params);

    /**
     * 新增权限标识
     *
     * @param params
     * @return
     */
    ApiResult add(Map<String, Object> params);

    /**
     * 查询权限是否被使用
     *
     * @param id
     * @return
     */
    ApiResult selectSecurityRoleId(Long id);

    /**
     * 查询当前权限信息
     *
     * @param id
     * @return
     */
    ApiResult findById(Long id);

    /**
     * 修改当前权限信息
     *
     * @param params
     * @return
     */
    ApiResult update(Map<String, Object> params);

    /**
     * 删除当前权限信息
     *
     * @param id
     * @return
     */
    ApiResult del(Long id);

    /**
     * 停用/恢复权限
     *
     * @param id
     * @return
     */
    ApiResult deactivate(Long id);
}
