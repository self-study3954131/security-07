package com.ncamc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.config.JwtProperties;
import com.ncamc.entity.LoginUser;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.DateConstant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.UserMapper;
import com.ncamc.service.UserService;
import com.ncamc.utils.JwtUtils;
import com.ncamc.utils.RedisCache;
import com.ncamc.entity.dto.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-07-05 10:26
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService
{
    private static final String current = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE_TIME));
    @Resource
    private UserMapper userMapper;

    @Resource
    private JwtProperties jwtProperties;

    @Resource
    private RedisCache redisCache;

    @Resource
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * 注册用户和新增用户信息
     *
     * @param user
     * @return
     */
    @Override
    public ApiResult register(User user)
    {
        if (StringUtils.isEmpty(user.getNickName()))// 判断用户名是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_NICKNAME);
        if (Constant.ADMIN.equals(user.getNickName()))// 判断用户名是否为管理员
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.NICKNAME_NOT_INPUT_ADMIN);
        if (Constant.admin.equals(user.getUsername()))// 判断用户名是否为admin
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.USERNAME_NOT_IS_ADMIN);
        if (StringUtils.isEmpty(user.getUsername()))// 判断账号是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_USERNAME);
        if (StringUtils.isEmpty(user.getPassword()))// 判断密码是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_PWD);
        // 查询所有用户
        Long count = userMapper.FindAllUser();
        // 给user对象复制
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setCreateTime(current);
        user.setUpdateTime(current);
        user.setCreateBy(tokenService.getLoginUser().getUser().getNickName());
        user.setId(count + 1);
        // 添加用户信息
        userMapper.insert(user);
        // 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK);
    }

    /**
     * 登录
     *
     * @param users
     * @return
     */
    @Override
    public ApiResult login(User users)
    {
        ApiResult apiResult;
        // 创建用户密码身份认证令盘
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(users.getUsername(), users.getPassword());
        // 这个方法会去调用UserDetailsServicesImpl
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        // 获取authenticate中返回的用户信息
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        // 判断是否为空 如果为空代表账号或者密码错误
        if (StringUtils.isEmpty(loginUser))
            // 返回错误信息
            return ApiResult.ok(HttpStatus.FOUND.value(), Constant.STR_ACCOUNT_OR_PASSWORD_ERROR);
        // 获取具体用户信息
        User user = loginUser.getUser();
        // 判断状态是否为0  0代表正常 1代表停用
        if (Objects.equals("0", user.getStatus()))
        {// true
            // 判断登录次数是否超过五次 如果超过停用账户
            if (user.getNumber() < 5)
            {// true
                // 添加登录时间
                Map<String, Object> map = new HashMap<>();
                map.put("name", user.getUsername());
                map.put("status", user.getStatus());
                userMapper.saveLoginTime(map);
                // 修改登录次数
                userMapper.updateNumberById(user.getNumber(), user.getId());
                // 将当前用户和token添加redis缓存
                String uid = user.getId().toString();
                // 通过ID生成token
                String token = JwtUtils.generateToken(uid, jwtProperties.getPrivateKey(), jwtProperties.getExpire());
                // 拼接key
                String key = Constant.LOGIN_USER + uid;
                // 将用户信息添加缓存
                redisCache.setCacheObject(key, loginUser);
                // 将token添加缓存
                redisCache.setCacheObject(Constant.LOGIN_TOKEN, token);
                // 返回结果
                apiResult = new ApiResult(HttpStatus.OK.value(), Constant.STR_LOGIN_OK, token);
            }
            else
            {// false
                // 设置状态字段为1
                user.setStatus(Constant.INT_ONE);
                // 修改登录这状态
                userMapper.updateStatusById(user.getStatus(), user.getId());
                // 返回错误信息
                apiResult = new ApiResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_EXCEED_LOGIN_NUMBER, Constant.STR_EMPTY);
            }
        }
        else
        {
            // false 返回错误信息
            apiResult = new ApiResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_STOP, Constant.STR_EMPTY);
        }
        // 返回结果
        return apiResult;
    }

    /**
     * 获取用户名称
     *
     * @return
     */
    @Override
    public ApiResult getUsername()
    {
        // 通过tokenService中的getLoginUser获取用户信息
        LoginUser loginUser = tokenService.getLoginUser();
        // 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, loginUser.getUser().getUsername());
    }

    /**
     * 获取用户最后登录时间
     *
     * @param apiResult
     * @return
     */
    @Override
    public ApiResult getLoginTime(ApiResult apiResult)
    {
        // 获取用户名
        String username = (String) apiResult.getData();
        // 返回结果
        return ApiResult.ok(Constant.STR_FIND_OK, userMapper.getLoginTime(username));
    }

    /**
     * 修改密码
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult updatePassword(Map<String, Object> params)
    {
        ApiResult apiResult;
        // 原密码
        String original_password = MapUtils.getString(params, "original_password");
        // 新密码
        String new_password = MapUtils.getString(params, "new_password");
        // 确认密码
        String confirm_password = MapUtils.getString(params, "confirm_password");

        if (StringUtils.isEmpty(original_password))// 判断原密码是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_ORIGINAL_PWD);
        if (StringUtils.isEmpty(new_password))// 判断新密码是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_NEW_PWD);
        if (StringUtils.isEmpty(confirm_password))// 判断确认密码是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_CONFIRM_PWD);

        // 通过tokenService获取用户信息
        User user = tokenService.getLoginUser().getUser();
        if (StringUtils.isEmpty(user))// 判断用户是否为空
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_NOT_USER_NEW_LOGIN);

        // 通过bCryptPasswordEncoder.matches匹配原密码和用户密码是否一致
        if (bCryptPasswordEncoder.matches(original_password, user.getPassword()))
        {// 一致
            // 判断新密码和原密码是否一致
            if (!Objects.equals(new_password, original_password))
            {// 不一致
                // 判断新密码和确认密码是否一致
                if (Objects.equals(new_password, confirm_password))
                {// 一致 修改密码
                    apiResult = new ApiResult(HttpStatus.OK.value(), Constant.STR_UPDATE_OK, userMapper.updatePasswordById(bCryptPasswordEncoder.encode(new_password), user.getId()));
                }
                else
                {// 不一致 返回错误信息
                    apiResult = new ApiResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_NEWPASSWORD_AND_CONFIRMPASSWORD_NOT_CONSISTENT);
                }
            }
            else
            {// 一致 返回错误信息
                apiResult = new ApiResult(HttpStatus.NOT_IMPLEMENTED.value(), Constant.STR_NEWPASSWORD_NOT_OPASSWORD_CONSISTENT);
            }
        }
        else
        {// 不一致 返回错误信息
            apiResult = new ApiResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_OPASSWORD_NOT_CONSISTENT);
        }
        // 返回结果
        return apiResult;
    }

    /**
     * 退出
     *
     * @return
     */
    @Override
    public ApiResult exit()
    {
        // 通过tokenService删除redis中的用户信息
        return tokenService.apiResult();
    }

    /**
     * 查询用户分页信息
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult listPage(Page<User> page, Map<String, Object> params)
    {
        String nickNames = tokenService.getLoginUser().getUser().getNickName();
        // 获取用户名
        String nickName = MapUtils.getString(params, "nickName");
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        // 判断用户名是否为空
        if (!StringUtils.isEmpty(nickName))
        {// 不为空 返回结果
            if (!Objects.equals(Constant.ADMIN, nickNames))
                queryWrapper.notIn(User::getNickName, Constant.ADMIN);
            // 添加条件 根据用户名模糊查询
            queryWrapper.like(User::getNickName, nickName);
            return ApiResult.ok(Constant.STR_FIND_OK, userMapper.selectPage(page, queryWrapper));
        }
        else
        {// 为空 查询所有用户信息 返回结果
            if (!Objects.equals(Constant.ADMIN, nickNames))
                queryWrapper.notIn(User::getNickName, Constant.ADMIN);
            return ApiResult.ok(Constant.STR_FIND_OK, userMapper.selectPage(page, queryWrapper));
        }
    }

    /**
     * 查询当前用户信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult selectById(Long id)
    {
        // 根据用户id查询用户信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, userMapper.selectById(id));
    }

    /**
     * 修改当前用户信息
     *
     * @param user
     * @return
     */
    @Override
    public ApiResult updateUser(User user)
    {
        // 创建UpdateWrapper
        UpdateWrapper updateWrapper = new UpdateWrapper<>();
        // 添加条件 匹配id
        updateWrapper.eq("id", user.getId());
        // 根据id查询用户信息
        User users = userMapper.selectById(user.getId());
        // 判断修改前用户密码和修改后用户密码是否一致 一致则跳过此步骤
        if (!Objects.equals(user.getPassword(), users.getPassword()))// 不一致 将修改后的密码加密赋值给当前用户
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        // 获取当前登录用户赋值给修改人
        user.setUpdateBy(tokenService.getLoginUser().getUser().getNickName());
        // 修改用户信息
        int count = userMapper.update(user, updateWrapper);
        // 判断是否修改成功
        if (count == 1)
        {// 成功 删除redis缓存中的用户信息 重新添加缓存 返回结果
            redisCache.deleteObject(Constant.CACHE_KEY_LOGIN + user.getId());
            redisCache.setCacheObject(Constant.CACHE_KEY_LOGIN + user.getId(), user);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_UPDATE_OK, count);
        }
        // 不成功 返回错误信息
        return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_UPDATE_ERROR);
    }

    /**
     * 根据ID删除该条数据
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult delUser(Long id)
    {
        // 获取用户信息
        User user = tokenService.getLoginUser().getUser();
        // 删除用户角色
        Integer count = userMapper.delUser(id);
        // 判断是否删除成功
        if (count == 1)
        {// 成功 删除当前用户 返回结果
            // 修改当前删除操作的修改人
            userMapper.updateBy(user.getNickName(), id);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, userMapper.deleteById(id));
        }
        // 不成功 返回错误信息
        return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_DEL_ERROR);
    }

}
