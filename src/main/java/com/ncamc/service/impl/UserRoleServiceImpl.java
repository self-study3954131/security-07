package com.ncamc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.entity.Role;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.UserMapper;
import com.ncamc.mapper.UserRoleMapper;
import com.ncamc.service.UserRoleService;
import com.ncamc.entity.dto.ApiResult;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, Role> implements UserRoleService
{
    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private TokenService tokenService;

    /**
     * 查询所有用户角色分页信息
     *
     * @param params
     * @param page
     * @return
     */
    @Override
    public ApiResult findAllUserRolePage(Page<User> page, Map<String, Object> params)
    {
        // 获取用户名称
        String nickName = MapUtils.getString(params, "nickName");
        // 获取当前用户名称
        String username = tokenService.getLoginUser().getUser().getNickName();
        // 查询用户角色信息
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, userRoleMapper.findAllUserRolePage(page, nickName, username));
    }

    /**
     * 查询所有角色信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult findAllRole(Long id)
    {
        // 创建queryWrapperUser
        LambdaQueryWrapper<User> queryWrapperUser = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapperUser.eq(User::getId, id);
        // 根据用户id查询用户
        User user = userMapper.selectOne(queryWrapperUser);
        // 查询当前用户所拥有的角色
        List<Role> roles = userRoleMapper.selectUserRole(id);
        // 查询所有角色
        List<Role> role = userRoleMapper.findAllRole();
        // 创建Map集合
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("roles", roles);
        map.put("role", role);
        // 返回map集合
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, map);
    }

    /**
     * 添加用户角色
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult saveUserRole(Map<String, Object> params)
    {
        // 获取用户名称
        String nickName = MapUtils.getString(params, "nickName");
        // 获取角色id
        int rid = MapUtils.getIntValue(params, "rid");
        // 根据角色id查询角色的状态
        String status = userRoleMapper.findRoleStatus(rid);
        if (Objects.equals("1", status))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_STOP);
        // 创建LambdaQueryWrapper并添加条件
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>().eq(User::getNickName, nickName);
        // 根据用户名查询用户id
        User user = userMapper.selectOne(queryWrapper);
        // 根据用户名查询roleID
        List<String> roleID = userRoleMapper.findRoleID(nickName);
        // 循环判断当前所选的角色id和当前用户所拥有的角色ID是否一致
        for (String rids : roleID)
        {
            if (rid == Integer.parseInt(rids))// 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_NAME_EXIST);
        }
        // 不一致 往sys_user_role表添加数据
        userRoleMapper.saveUserRole(user.getId(), rid);
        //  返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK);
    }

    /**
     * 查询当前用户是否拥有角色
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult selectUserRoleByIdU(Long id)
    {
        // 根据用户id查询角色名称
        Integer count = userRoleMapper.selectUserRoleById(id);
        // 判断角色名称是否为空
        if (count == 0)// 为空 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_USER_NOT_ROLE);
        // 不为空 返回成功
        return ApiResult.ok();
    }

    /**
     * 修改当前角色信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult updateUserRole(Map<String, Object> params) {
        // 获取用户id
        int uid = MapUtils.getIntValue(params, "id");
        // 获取角色id
        int rid = MapUtils.getIntValue(params, "rid");
        // 获取用户角色id
        int urid = MapUtils.getIntValue(params, "urid");

        if (urid == 0)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SELECT_USER_ROLE);
        if (rid == 0)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SELECT_ROLE);

        // 根据用户id查询用户名称
        String userName = userRoleMapper.selectUserName(uid);
        // 根据用户角色id查询角色名称
        String roleName = userRoleMapper.selectRoleName(urid);
        if (Objects.equals(Constant.ADMIN, userName) && Objects.equals(Constant.Admin, roleName))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_NAME_IS_ADMIN_NOT_UPDATE);

        if (urid == rid)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_ALIKE);

        // 根据角色id查询角色的状态
        String status = userRoleMapper.findRoleStatus(rid);
        if (Objects.equals(Constant.INT_ONE, status))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_STOP);

        // 查询当前用户所拥有的角色
        List<String> roleByIds = userRoleMapper.selectUserRoleByIds(uid);
        // 循环判断当前所选择的角色id是否和当前用户所拥有的角色ID一致
        for (String roleId : roleByIds) {
            if (rid == Integer.parseInt(roleId))
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.USER_EXIST_ROLE);
        }
        // 修改sys_user_role关联
        userRoleMapper.updateUserRole(uid, rid, urid);
        // 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK);
    }

    /**
     * 删除当前用户角色
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult delUserRole(Map<String, Object> params)
    {
        // 获取uid
        int uid = MapUtils.getIntValue(params, "id");
        // 获取rid
        int rid = MapUtils.getIntValue(params, "rid");
        // 删除用户角色 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, userRoleMapper.delUserRole(uid, rid));
    }
}
