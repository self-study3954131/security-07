package com.ncamc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.entity.Role;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.DateConstant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.RoleMapper;
import com.ncamc.mapper.UserRoleMapper;
import com.ncamc.service.RoleService;
import com.ncamc.entity.dto.ApiResult;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService
{
    private static final String current = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE_TIME));
    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private TokenService tokenService;

    /**
     * 查询角色分页信息
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult findAllPageRole(Page<Role> page, Map<String, Object> params)
    {
        String nickName = tokenService.getLoginUser().getUser().getNickName();
        // 创建 LambdaQueryWrapper
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        // 获取角色名称 roleName
        String roleName = MapUtils.getString(params, "roleName");
        // 判断roleName是否为空
        if (!StringUtils.isEmpty(roleName))
        {// 不为空 添加条件匹配角色名称模糊查询 返回结果
            if (!Objects.equals(Constant.ADMIN, nickName))
                queryWrapper.notIn(Role::getRoleName, Constant.Admin);
            queryWrapper.like(Role::getRoleName, roleName);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, roleMapper.selectPage(page, queryWrapper));
        }
        else
        {
            if (!Objects.equals(Constant.ADMIN, nickName))
                queryWrapper.notIn(Role::getRoleName, Constant.Admin);
            // 为空 查询所有角色信息
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, roleMapper.selectPage(page, queryWrapper));
        }
    }

    /**
     * 新增角色信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult saveRole(Map<String, Object> params)
    {
        // 获取当前用户信息
        User user = tokenService.getLoginUser().getUser();
        // 获取角色名称
        String roleName = MapUtils.getString(params, "roleName");
        if (StringUtils.isEmpty(roleName))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_ROLE_NAME);
        // 初始化变量
        String roleKey = null;
        // 将roleName转换为字符
        char[] chars = roleName.toCharArray();
        for (char charName : chars)
        {
            // 判断charName是否在 A——Z 之间
            if (charName >= 'A' && charName <= 'Z') {// 在 A——Z 之间将大写字符转为小写
                roleKey = roleName.toLowerCase();
            }
            else
            {// 不再 A——Z 之间 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_NAME_MAST_BIG);
            }
        }
        //查询所有角色
        List<Role> roleList = userRoleMapper.findAllRole();
        for (Role role : roleList)
        {
            // 判断新增角色名称和原角色名称是是否一致
            if (Objects.equals(roleName, role.getRoleName()))// 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_NAME_EXIST);
        }
        // 查询所有角色数量
        Long count = roleMapper.selectCount();
        // 创建Role对象 并赋值
        Role role = new Role();
        role.setId(count + 1);
        role.setRoleName(roleName);
        role.setRoleKey(roleKey);
        role.setCreateTime(current);
        role.setUpdateTime(current);
        role.setCreateBy(user.getNickName());
        role.setUpdateBy(user.getNickName());
        // 添加角色信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK, roleMapper.insert(role));
    }

    /**
     * 查询角色是否被使用
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult selectRoleUserById(Long id)
    {
        // 根据rid查询用户角色
        List<String> userName = roleMapper.selectUserRole(id);
        // 判断uid是否大于0
        if (userName.size() > 0)//大于0 代表该角色正在被使用
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_YOU_NICKNAME_SY + userName + Constant.USE);
        return ApiResult.ok();
    }

    /**
     * 查询当前角色信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult findRoleByID(Long id)
    {
        // 根据角色id查询角色信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, roleMapper.selectById(id));
    }

    /**
     * 修改当前权限信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult updateRole(Map<String, Object> params)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        // 获取用户信息
        User user = tokenService.getLoginUser().getUser();
        // 获取角色名称
        String roleName = MapUtils.getString(params, "roleName");
        // 获取角色id
        String id = MapUtils.getString(params, "id");

        if (Objects.equals(Constant.Admin, roleName))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_NAME_IS_ADMIN_NOT_UPDATE);

        // 根据角色ID查询角色信息
        Role roles = roleMapper.selectById(id);
        // 判断角色名称是否匹配
        if (Objects.equals(roleName, roles.getRoleName()))// 匹配 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_NAME_XT);
        // 查询所有角色
        List<Role> roleList = userRoleMapper.findAllRole();
        for (Role role : roleList)
        {
            // 判断修改角色名称和原角色名称是是否一致
            if (Objects.equals(roleName, role.getRoleName()))
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ROLE_NAME_XT);
        }
        // 初始化变量
        String roleKey = null;
        // 将roleName转换为字符
        char[] chars = roleName.toCharArray();
        for (char charName : chars) {
            //判断charName是否在 A——Z 之间
            if (charName >= 'A' && charName <= 'Z') { //在 A——Z 之间将大写字符转为小写
                roleKey = roleName.toLowerCase();
            }
            else
            {// 不再 A——Z 之间 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_NAME_MAST_BIG);
            }
        }
        // 创建Role对象 并赋值
        Role role = new Role();
        role.setRoleName(roleName);
        role.setRoleKey(roleKey);
        role.setUpdateBy(current);
        role.setUpdateBy(user.getNickName());
        // 添加条件
        queryWrapper.eq(Role::getId, id);
        // 修改角色信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK, roleMapper.update(role, queryWrapper));
    }

    /**
     * 删除当前角色信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult delRole(Long id)
    {
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, roleMapper.deleteById(id));
    }

    /**
     * 停用  恢复
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult deactivate(Long id)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapper.eq(Role::getId, id);
        // 查询角色信息
        Role roles = roleMapper.selectOne(queryWrapper);
        // 创建Role对象
        Role role = new Role();
        // 判断当前状态是否为0 修改状态为1
        if (Objects.equals("0", roles.getStatus()))
        {
            role.setStatus("1");
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEACTIVATE_OK, roleMapper.update(role, queryWrapper));
        }
        else
        {
            role.setStatus("0");
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_RESTORE_OK, roleMapper.update(role, queryWrapper));
        }
    }
}
