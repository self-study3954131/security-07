package com.ncamc.service.impl;

import com.ncamc.mapper.EchartsMapper;
import com.ncamc.service.EchartsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author : hugaoqiang 2023-07-13
 */
@Service
public class EchartsServiceImpl implements EchartsService
{

    @Resource
    private EchartsMapper echartsMapper;

    @Override
    public Map<String, Object> findAll()
    {
        Map<String, Object> map = new HashMap<>();

        Map<String, Integer> charts = echartsMapper.charts();
        Map<String, Integer> components = echartsMapper.components();
        Map<String, Object> builderJson = new HashMap<>();
        Map<String, Integer> downloadJson = echartsMapper.downloadJson();
        Map<String, Integer> themeJson = echartsMapper.themeJson();

        int chartsMax = 0;
        int componentsMax = 0;

        for (Map.Entry<String, Integer> chartsEntry : charts.entrySet())
        {
            if (chartsMax < chartsEntry.getValue())
                chartsMax = chartsEntry.getValue();
        }

        for (Map.Entry<String, Integer> componentsEntry : components.entrySet())
         {
            if (componentsMax < componentsEntry.getValue())
                componentsMax = componentsEntry.getValue();
        }

        // 最大值
        int all = Math.max(chartsMax, componentsMax);

        builderJson.put("all", all);
        builderJson.put("charts", charts);
        builderJson.put("components", components);
        builderJson.put("ie", 9744);

        map.put("builderJson", builderJson);
        map.put("downloadJson", downloadJson);
        map.put("themeJson", themeJson);
        return map;
    }
}
