package com.ncamc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.entity.Menu;
import com.ncamc.entity.Role;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.LogConstant;
import com.ncamc.mapper.RoleMapper;
import com.ncamc.mapper.RoleSecurityMapper;
import com.ncamc.service.RoleSecurityService;
import com.ncamc.entity.dto.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class RoleSecurityServiceImpl extends ServiceImpl<RoleSecurityMapper, Menu> implements RoleSecurityService
{
    @Resource
    private RoleSecurityMapper roleSecurityMapper;

    @Resource
    private RoleMapper roleMapper;

    /**
     * 查询角色权限分页信息
     *
     * @param params
     * @param page
     * @return
     */
    @Override
    public ApiResult findAllRoleSecurityPage(Page<User> page, Map<String, Object> params)
    {
        Page<Map<String, Object>> roleSecurityPage = roleSecurityMapper.findAllRoleSecurityPage(page, MapUtils.getString(params, "roleName"));
        log.info(LogConstant.FIND_ROLE_PAGE_INFO, roleSecurityPage);
        return ApiResult.ok(
                HttpStatus.OK.value(),
                Constant.STR_FIND_OK,
                roleSecurityPage);
    }

    /**
     * 查询当前所选用户和权限信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult findRoleSecurityByID(Long id)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapper.eq(Role::getId, id);
        // 查询角色信息
        Role role = roleMapper.selectOne(queryWrapper);
        log.info(LogConstant.FIND_ROLE_INFO, role);
        // 根据角色id查询所拥有的权限id
        List<Menu> menus = roleSecurityMapper.selectRoleMenuByIds(id);
        log.info(LogConstant.FIND_ROLE_SECURITY_BY_ID, menus);
        // 根据角色名称查询权限集合
        List<Menu> menu = roleSecurityMapper.getAllMenu();
        log.info(LogConstant.FIND_SECURITY_COLLATION_BY_NAME, menu);
        // 创建Map集合
        Map<String, Object> map = new HashMap<>();
        map.put("role", role);
        map.put("menus", menus);
        map.put("menu", menu);
        log.info(LogConstant.MAP_COLLATION, map);
        // 返回map集合
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, map);
    }

    /**
     * 新增用户权限信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult saveRoleSecurity(Map<String, Object> params)
    {
        // 获取角色id
        int rid = MapUtils.getIntValue(params, "id");
        // 获取权限id
        int mid = MapUtils.getIntValue(params, "mid");
        // 判断选择的权限名称ID是否为0
        if (mid == 0)// 为0 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SELECT_ROLE_SECURITY_NAME);
        // 根据权限id查询权限的状态
        String status = roleSecurityMapper.findSecurityStatus(mid);
        log.info(LogConstant.FIND_SECURITY_STATUS_BY_SECURITY_ID, status);
        if (Objects.equals("1", status))
        {
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_STOP_SELECT_SECURITY_NAME);
        }
        // 不为0 查询当前角色所拥有的权限ID
        List<String> menuById = roleSecurityMapper.selectRoleMenuById((long) rid);
        log.info(LogConstant.FIND_ROLE_SECURITY_ID, menuById);
        // 循环判断当前所选择的权限ID是否和当前角色所用有的权限ID一致
        for (String menuId : menuById)
        {
            if (mid == Integer.parseInt(menuId))// 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_EXIST_SECURITY_SELECT_SECURITY_NAME);
        }
        // 不一致 往sys_role_menu表添加数据
        roleSecurityMapper.saveRoleMenu(rid, mid);
        // 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK);
    }

    /**
     * 查询当前角色权限
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult selectRoleMenuById(Long id)
    {
        // 根据角色id查询权限id
        List<String> mid = roleSecurityMapper.selectRoleMenuById(id);
        log.info(LogConstant.FIND_SECURITY_ID_BY_ROLE_ID, mid);
        // 判断权限id是否等于0
        if (mid.size() == 0)// 等于0 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_NOT_SECURITY_ADD_SECURITY);
        // 不等于0 正常返回 200
        return ApiResult.ok();
    }

    /**
     * 修改当前角色权限信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult updateRoleSecurity(Map<String, Object> params)
    {
        // 获取角色id
        int rid = MapUtils.getIntValue(params, "id");
        // 获取角色权限id
        int rmid = MapUtils.getIntValue(params, "rmid");
        // 获取权限ID
        int mid = MapUtils.getIntValue(params, "mid");

        if (rmid == 0)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SELECT_ROLE_SECURITY_NAME);
        if (mid == 0)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SELECT_SECURITY_NAME);

        // 根据角色id查询角色名称
        String roleName = roleSecurityMapper.selectRoleName(rid);
        log.info(LogConstant.FIND_ROLE_NAME_BY_RID, roleName);
        // 根据角色id查询权限名称
        String menuName = roleSecurityMapper.selectMenuName(rmid);
        log.info(LogConstant.FIND_SECURITY_NAME_BY_RMID, menuName);
        if (Objects.equals(Constant.Admin, roleName) && Objects.equals(Constant.DEPT, menuName))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_NAME_IS_ADMIN_NOT_UPDATE);

        if (rmid == mid)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_NAME_ALIKE);

        // 根据权限id查询权限的状态
        String status = roleSecurityMapper.findSecurityStatus(mid);
        if (Objects.equals("1", status))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_STOP_SELECT_SECURITY_NAME);

        // 查询当前角色所拥有的权限ID
        List<String> menuById = roleSecurityMapper.selectRoleMenuById((long) rid);
        log.info(LogConstant.FIND_ROLE_SECURITY_ID, menuById);
        // 循环判断当前所选择的权限ID是否和当前角色所用有的权限ID一致
        for (String menuId : menuById)
        {
            if (mid == Integer.parseInt(menuId))// 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.ROLE_EXIST_SECURITY_SELECT_SECURITY_NAME);
        }

        roleSecurityMapper.updateRoleMenu(rid, mid, rmid);
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_UPDATE_OK);
    }

    /**
     * 删除当前角色权限
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult delRoleSecurity(Map<String, Object> params)
    {
        // 获取rid
        int rid = MapUtils.getIntValue(params, "id");
        // 获取mid
        int mid = MapUtils.getIntValue(params, "mid");
        log.info(LogConstant.DEL_ROLE_SECURITY, roleSecurityMapper.delUserSecurityByUID(rid, mid));
        // 删除当前角色权限 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, roleSecurityMapper.delUserSecurityByUID(rid, mid));
    }
}
