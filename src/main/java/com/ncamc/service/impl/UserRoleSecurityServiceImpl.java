package com.ncamc.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.internal.Constant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.UserRoleSecurityMapper;
import com.ncamc.service.UserRoleSecurityService;
import com.ncamc.entity.dto.ApiResult;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class UserRoleSecurityServiceImpl implements UserRoleSecurityService
{
    @Resource
    private UserRoleSecurityMapper userRoleSecurityMapper;

    @Resource
    private TokenService tokenService;

    /**
     * 查询用户角色权限分页信息
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult findAllUserRoleSecurityPage(Page<Menu> page, Map<String, Object> params)
    {
        // 获取用户信息
        String username = tokenService.getLoginUser().getUser().getNickName();
        // 获取用户名称
        String nickName = MapUtils.getString(params, "nickName");
        // 查询所有用户角色权限信息
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, userRoleSecurityMapper.findAllUserRoleSecurityPage(page, nickName, username));
    }
}
