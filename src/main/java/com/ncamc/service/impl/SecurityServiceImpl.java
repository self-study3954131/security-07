package com.ncamc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.DateConstant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.SecurityMapper;
import com.ncamc.service.SecurityService;
import com.ncamc.entity.dto.ApiResult;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class SecurityServiceImpl implements SecurityService
{
    private static final String current = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE_TIME));
    @Resource
    private TokenService tokenService;

    @Resource
    private SecurityMapper securityMapper;

    /**
     * 查询权限分页信息
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult list(Page<Menu> page, Map<String, Object> params)
    {
        String nickName = tokenService.getLoginUser().getUser().getNickName();
        // 获取 权限名称menuName
        String menuName = MapUtils.getString(params, "menuName");
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        // 判断menuName是否为空
        if (StringUtils.isEmpty(menuName))
        {// 为空 查询所有权限信息 返回结果
            if (!Objects.equals(Constant.ADMIN, nickName))
                queryWrapper.notIn(Menu::getMenuName, Constant.DEPT);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, securityMapper.selectPage(page, queryWrapper));
        }
        else
        {// 不为空 根据权限名称模糊查询 返回结果
            if (!Objects.equals(Constant.ADMIN, nickName))
                queryWrapper.notIn(Menu::getMenuName, Constant.DEPT);
            queryWrapper.like(Menu::getMenuName, menuName);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, securityMapper.selectPage(page, queryWrapper));
        }
    }

    /**
     * 新增权限标识
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult add(Map<String, Object> params)
    {
        // 获取用户信息
        User user = tokenService.getLoginUser().getUser();
        // 判断用户是否为空
        if (StringUtils.isEmpty(user))
        {
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_NOT_USER_NEW_LOGIN);
        }
        // 获取权限名称
        String menuName = MapUtils.getString(params, "menuName");
        // 获取权限标识
        String path = MapUtils.getString(params, "path");
        // 判断menuName是否为空
        if (StringUtils.isEmpty(menuName))// 空 返回错误信息 null
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.INPUT_SECURITY_NAME);
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        // 查询所有权限信息
        List<Menu> menus = securityMapper.selectList(queryWrapper);
        for (Menu menu : menus)
        {
            // 判断新增权限名称和原权限名称是否一致
            if (Objects.equals(menuName, menu.getMenuName()))
            {// 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_SECURITY_NAME_EXITS);
            }
            // 判断新增路由地址和原路由地址是否一致
            if (path.equals(menu.getPath())) // 一致 返回错误信息
                return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_SECURITY_BS_EXITS);
        }
        // 拼接权限标识
        String system = "system:";
        String list = ":list";
        String perms = system + path + list;
        // 判断权限长度是否小于15位 小于 返回错误信息
        if (perms.length() < 15 || perms.length() > 16)
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_MUST_IS_TREE_FOUR);
        // 将权限标识中的:替换为/
        String component_ = perms.replace(":", "/");
        // 将权限标识中的list转换为index
        String index = "index";
        list = "list";
        String component = component_.replace(list, index);
        // 查询所有权限数量
        Long count = securityMapper.selectCount();
        // 创建Menu对象 并赋值
        Menu menu = new Menu();
        menu.setId(count + 1);
        menu.setMenuName(menuName);
        menu.setPath(path);
        menu.setComponent(component);
        menu.setPerms(perms);
        menu.setIcon("#");
        menu.setCreateBy(user.getNickName());
        menu.setUpdateBy(user.getNickName());
        menu.setCreateTime(current);
        menu.setUpdateTime(current);
        // 添加权限信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK, securityMapper.insert(menu));
    }

    /**
     * 查询权限是否被使用
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult selectSecurityRoleId(Long id)
    {
        // 根据mid查询角色权限
        List<String> rid = securityMapper.selectRoleMenu(id);
        // 判断rid是否大于0
        if (rid.size() > 0) // 大于0 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_SECURITY_YOU_NICKNAME_SY + rid + Constant.USE);
        return ApiResult.ok();
    }

    /**
     * 查询当前权限信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult findById(Long id)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapper.eq(Menu::getId, id);
        // 查询当前权限信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, securityMapper.selectOne(queryWrapper));
    }

    /**
     * 修改当前权限信息
     *
     * @param params
     * @return
     */
    @Override
    public ApiResult update(Map<String, Object> params)
    {
        // 获取当前权限id
        Long mid = MapUtils.getLongValue(params, "id");
        // 获取权限名称
        String menuNameNew = MapUtils.getString(params, "menuName");
        // 根据权限id查询权限名称
        String menuName = securityMapper.selectMenuName(mid);

        if (Objects.equals(Constant.DEPT, menuName))
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_NAME_IS_ADMIN_NOT_UPDATE);

        // 判断当前权限名称和修改后的权限名称是否一致
        if (Objects.equals(menuName, menuNameNew))//一致 返回错误信息
            return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.SECURITY_NAME_NOT_ALIKE);
        // 创建Menu对象 并赋值
        Menu menu = new Menu();
        menu.setMenuName(menuNameNew);
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapper.eq(Menu::getId, MapUtils.getIntValue(params, "id"));
        // 修改权限信息 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_UPDATE_OK, securityMapper.update(menu, queryWrapper));
    }

    /**
     * 删除当前权限信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult del(Long id)
    {
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, securityMapper.deleteById(id));
    }

    /**
     * 停用/恢复权限
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult deactivate(Long id)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        // 添加条件
        queryWrapper.eq(Menu::getId, id);
        // 查询当前权限信息
        Menu menus = securityMapper.selectOne(queryWrapper);
        // 创建Menu对象
        Menu menu = new Menu();
        // 判断当前状态是否为0 修改状态为1
        if (Objects.equals(Constant.INT_ZERO, menus.getStatus()))
        {
            menu.setStatus(Constant.INT_ONE);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEACTIVATE_OK, securityMapper.update(menu, queryWrapper));
        }
        else
        {
            menu.setStatus(Constant.INT_ZERO);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_RESTORE_OK, securityMapper.update(menu, queryWrapper));
        }
    }
}
