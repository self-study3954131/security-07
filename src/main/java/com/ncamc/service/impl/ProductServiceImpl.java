package com.ncamc.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.ncamc.entity.EchartsBean;
import com.ncamc.entity.Product;
import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.internal.DateConstant;
import com.ncamc.internal.LogConstant;
import com.ncamc.internal.TokenService;
import com.ncamc.mapper.ProductMapper;
import com.ncamc.service.ProductService;
import com.ncamc.utils.RedisCache;
import com.ncamc.entity.dto.ApiResult;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-07-08 09:55
 */
@Slf4j
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService
{
    private static final String newDate = LocalDate.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE));
    private static final String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE_TIME));
    @Resource
    private ProductMapper productMapper;
    @Resource
    private RedisCache redisCache;
    @Resource
    private TokenService tokenService;

    /**
     * 多表分页模糊条件查询
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult getProductList(Page<Map<String, Object>> page, Map<String, Object> params)
    {
        Page<Map<String, Object>> res = null;

        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();

        String username = MapUtils.getString(params, "username");
        String id = MapUtils.getString(params, "id");

        if (Strings.isNotBlank(username) && !(Strings.isNotBlank(id)))
        {
            productQueryWrapper.lambda().like(Product::getPrdName, username);
            res = this.baseMapper.page(page, productQueryWrapper);
        }
        if (Strings.isNotBlank(id) && !(Strings.isNotBlank(username)))
        {
            userQueryWrapper.lambda().eq(User::getId, id);
            res = this.baseMapper.pages(page, userQueryWrapper);
        }
        if (Strings.isNotBlank(id) && Strings.isNotBlank(username))
        {
            productQueryWrapper.lambda().like(Product::getPrdName, username);
            res = this.baseMapper.pageByIdAndLikeName(page, productQueryWrapper, Integer.parseInt(id));
        }
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, res);
    }

    /**
     * 查询产品分页信息
     *
     * @param page
     * @param params
     * @return
     */
    @Override
    public ApiResult listPage(Page<Product> page, Map<String, Object> params)
    {
        // 获取产品名称/机构名称
        String prdIns = MapUtils.getString(params, "prdIns");
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        // 判断产品名称/机构名称是否为空
        if (StringUtils.isEmpty(prdIns))
        {// 为空 查询所有产品信息 返回结果
            Page<Product> productPage = productMapper.selectPage(page, queryWrapper);
            List<Product> products = productPage.getRecords();
            log.info(LogConstant.FIND_ALL_PRD_PAGE_INFO, products.toString());
            for (Product product : products)
            {
                // 拼接key
                String key = Constant.CACHE_KEY_USER + product.getId();
                log.info(LogConstant.SPLICE_KEY, key);
                redisCache.deleteObject(key);
                redisCache.setCacheObject(key, product);
            }
            return ApiResult.ok(Constant.STR_FIND_OK, productPage);
        }
        else
        {// 不为空 根据产品名称/机构名称模糊查询 返回结果
            queryWrapper.like(Product::getPrdName, prdIns).or().like(Product::getInsName, prdIns);
            Page<Product> productPage = productMapper.selectPage(page, queryWrapper);
            List<Product> products = productPage.getRecords();
            log.info(LogConstant.FIND_PRD_INS_NAME_LIKE, products.toString());
            for (Product product : products)
            {
                // 拼接key
                String key = Constant.CACHE_KEY_USER + product.getId();
                log.info(LogConstant.SPLICE_KEY, key);
                redisCache.deleteObject(key);
                redisCache.setCacheObject(key, product);
            }
            return ApiResult.ok(Constant.STR_FIND_OK, productPage);
        }
    }

    /**
     * 新增产品信息
     *
     * @param product
     * @return
     */
    @Override
    public ApiResult add(Product product)
    {
        // 创建LambdaQueryWrapper
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        // 查询产品总条数
        Integer counts = productMapper.selectCount(queryWrapper);
        log.info(LogConstant.FIND_PRD_COUNTS, counts);
        // 为Product赋值
        product.setId(counts + 1);
        product.setNewDate(newDate);
        product.setCreateTime(createTime);
        product.setCreateBy(tokenService.getLoginUser().getUser().getNickName());
        log.info(LogConstant.PRD_ASSIGN_RETURN, product);
        // 添加产品信息
        int count = productMapper.insert(product);
        // 判断是否添加成功
        if (count == 1)
        {// 成功 根据产品id查寻产品信息 添加redis缓存 返回结果
            product = productMapper.selectById(product.getId());
            log.info(LogConstant.ADD_REDIS_RETURN, product);
            redisCache.setCacheObject(Constant.CACHE_KEY_USER + product.getId(), product);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_ADD_OK);
        }
        // 不成功 返回错误信息
        return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_ADD_ERROR);
    }

    /**
     * 新增产品信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult findById(Long id)
    {
        Product product;
        // 拼接key
        String key = Constant.CACHE_KEY_USER + id;
        log.info(LogConstant.SPLICE_KEY, key);
        // 根据key查询当前产品信息
        product = redisCache.getCacheObject(key);
        log.info(LogConstant.FIND_PRD_INFO_BY_KEY, product);
        // 判断产品是否为空
        if (StringUtils.isEmpty(product))
        {// 为空 根据ID查询产品详情
            product = productMapper.selectById(id);
            log.info(LogConstant.FIND_PRD_INFO_BY_ID, product);
            // 判断产品是否为空
            if (StringUtils.isEmpty(product)) return null;// 为空 返回null
            // 不为空 重新将产品信息添加到缓存
            redisCache.setCacheObject(key, product);
        }
        // 不为空 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_FIND_OK, product);
    }

    /**
     * 修改当前产品信息
     *
     * @param product
     * @return
     */
    @Override
    public ApiResult updateProduct(Product product)
    {
        // 创建UpdateWrapper
        UpdateWrapper wrapper = new UpdateWrapper<>();
        // 添加条件
        wrapper.eq("id", product.getId());
        // 赋值
        product.setUpdateBy(tokenService.getLoginUser().getUser().getNickName());
        product.setUpdateTime(createTime);
        log.info(LogConstant.ASSIGN_RETURN, product);
        // 修改产品信息
        int count = productMapper.update(product, wrapper);
        log.info(LogConstant.UPDATE_PRD_INFO_RETURN, count);
        // 判断是否修改成功
        if (count == 1)
        {// 成功 删除redis缓存产品 重新添加 返回结果
            String key = Constant.CACHE_KEY_USER + product.getId();
            log.info(LogConstant.SPLICE_KEY, key);
            redisCache.deleteObject(key);
            redisCache.setCacheObject(key, product);
            return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_UPDATE_OK);
        }
        // 不成功 返回错误信息
        return ApiResult.ok(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constant.STR_UPDATE_ERROR);
    }

    /**
     * 删除当前产品信息
     *
     * @param id
     * @return
     */
    @Override
    public ApiResult deleteByPrimaryKey(Long id)
    {
        // 获取当前用户登录信息
        User user = tokenService.getLoginUser().getUser();
        log.info(LogConstant.USER_LOGIN_INFO, user);
        // 修改当前删除操作的修改人
        productMapper.updateBy(user.getNickName(), id);
        // 删除redis缓存信息
        redisCache.deleteObject(Constant.CACHE_KEY_USER + id);
        // 返回结果
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_DEL_OK, productMapper.deleteById(id));
    }

    /**
     * 柱状图
     *
     * @return
     */
    @Override
    public Map<String, Object> getDatas()
    {
        Map<String, Object> map = productMapper.selectXY1();
        EchartsBean echartsBean = new EchartsBean();

        Map<String, Object> maps = new HashMap<>();
        Set<Map.Entry<String, Object>> entries = map.entrySet();
        List<String> listX = new ArrayList<>();
        List<Integer> listY = new ArrayList<>();
        for (Map.Entry<String, Object> entry : entries)
        {
            listX.add(entry.getKey());
            listY.add(((Number) entry.getValue()).intValue());
        }
        echartsBean.setxAxisCategory(listX);
        echartsBean.setDatas(listY);

        maps.put("data", echartsBean);
        maps.put("name", Constant.PRD_DATA);
        maps.put("text", Constant.TEXT);
        return maps;
    }

    /**
     * 饼图
     *
     * @return
     */
    @Override
    public Map<String, Object> getDatas2()
    {
        Map<String, Object> map = productMapper.selectXY2();

        Map<String, Object> maps = new HashMap<>();
        Map<String, Object> mapXY = new HashMap<>();
        List<String> data = new ArrayList<>();

        Set<Map.Entry<String, Object>> entries = map.entrySet();
        for (Map.Entry<String, Object> entry : entries)
        {
            mapXY.put("name", entry.getKey());
            mapXY.put("value", entry.getValue());
            data.add(JSONObject.toJSONString(mapXY));
        }

        maps.put("data", JSON.parse(data.toString()));
        maps.put("name", Constant.PRD_DATA);
        return maps;
    }

    /**
     * @param file
     */
    @SneakyThrows
    @Override
    public void upExcel(MultipartFile file)
    {
        try
        {
            // 获取文件名称
            String filename = file.getOriginalFilename();
            // 获取文件格式
            String extension = filename.substring(filename.lastIndexOf(".") + 1);
            // 判断是否包含extension文件  不包含 返回错误信息
            if (!Lists.newArrayList(Constant.xls, Constant.xlsx, Constant.XLS, Constant.XLSX).contains(extension))
                throw new ServletException(Constant.FILE_FORMAT + extension);

            // 创建workbook对象，读取整个文档
            XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
            // 读取页脚sheet
            XSSFSheet sheetAt = wb.getSheetAt(0);
            // 循环读取某一行
            int index = 0;
            for (Row row : sheetAt)
            {
                // 读取每一行的单元格
                if (index == 0 || index == 1)
                {
                    index++;
                    continue;
                }
                // 查询产品总数量
                int count = productMapper.selectCount(new QueryWrapper<>());
                // 创建一个产品对象
                Product product = new Product();
                // 将Excel表中单元格的值与产品对象的值对应
                product.setId(count + 1);
                product.setPrdName(row.getCell(1).getStringCellValue());
                product.setPrdDm(row.getCell(2).getStringCellValue());
                product.setNet(row.getCell(3).getStringCellValue());
                product.setFbalance(row.getCell(4).getStringCellValue());
                product.setFavalable(row.getCell(5).getStringCellValue());
                product.setInsName(row.getCell(6).getStringCellValue());
                product.setInsDm(row.getCell(7).getStringCellValue());
                product.setNewDate(row.getCell(8).getStringCellValue());
                product.setCreateBy(row.getCell(9).getStringCellValue());
                product.setCreateTime(row.getCell(10).getStringCellValue());

                String updateBy = row.getCell(11).getStringCellValue();
                if (updateBy == null || "".equals(updateBy))
                {
                    product.setUpdateBy(null);
                }
                else
                {
                    product.setUpdateBy(updateBy);
                }

                String updateTime = row.getCell(12).getStringCellValue();
                if (updateTime == null || "".equals(updateTime))
                {
                    product.setUpdateTime(null);
                }
                else
                {
                    product.setUpdateTime(updateTime);
                }

                productMapper.insert(product);
            }
        }
        catch (IOException e)
        {
            throw new ServletException(Constant.UPLOAD_FAILURE);
        }
    }

}
