package com.ncamc.internal;

import com.ncamc.config.JwtProperties;
import com.ncamc.entity.LoginUser;
import com.ncamc.utils.JwtUtils;
import com.ncamc.utils.RedisCache;
import com.ncamc.entity.dto.ApiResult;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Component
public class TokenService
{
    @Resource
    private JwtProperties jwtProperties;

    @Resource
    private RedisCache redisCache;

    @Resource
    private HttpServletRequest request;

    /**
     * 获取LoginUser
     *
     * @return
     */
    public LoginUser getLoginUser()
    {
        String key = getRedisKey();
        LoginUser loginUser = redisCache.getCacheObject(key);
        if (Objects.isNull(loginUser))
        {
            throw new RuntimeException(Constant.STR_NOT_USER_NEW_LOGIN);
        }
        return loginUser;
    }

    /**
     * 删除缓存中的key
     *
     * @return
     */
    public ApiResult apiResult()
    {
        String key = getRedisKey();
        redisCache.deleteObject(key);
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_OUT_OK);
    }

    /**
     * 获取缓存中存储用户的key
     *
     * @return
     */
    public String getRedisKey()
    {
        String token = request.getHeader(Constant.JWT_HEADER_TOKEN);
        if (!StringUtils.hasText(token))
        {
            throw new RuntimeException(JwtConstant.STR_NOT_DATA);
        }
        Long uid;
        try
        {
            uid = JwtUtils.getInfoFromId(token, jwtProperties.getPublicKey());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(JwtConstant.STR_ANALYSIS_ERROR);
        }
        return Constant.LOGIN_USER + uid;
    }

}
