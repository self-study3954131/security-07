package com.ncamc.internal;

/**
 * 日志常量
 */
public interface LogConstant
{
    // TODO productServiceImpl
    /**
     * 查询产品分页信息-查询所有产品信息结果 products: {}
     */
    String FIND_ALL_PRD_PAGE_INFO = "查询产品分页信息-查询所有产品信息结果 products: {}";

    /**
     * 查询产品分页信息-拼接key: {}
     */
    String SPLICE_KEY = "拼接key: {}";

    /**
     * 查询产品分页信息-根据产品名称/机构名称模糊查询结果 products: {}
     */
    String FIND_PRD_INS_NAME_LIKE = "查询产品分页信息-根据产品名称/机构名称模糊查询结果 products: {}";

    /**
     * 新增产品信息-查询产品总条数 counts: {}
     */
    String FIND_PRD_COUNTS = "新增产品信息-查询产品总条数 counts: {}";

    /**
     * 新增产品信息-product赋值结果: {}
     */
    String PRD_ASSIGN_RETURN = "新增产品信息-product赋值结果: {}";

    /**
     * 新增产品信息-添加redis缓存结果: {}
     */
    String ADD_REDIS_RETURN = "新增产品信息-添加redis缓存结果: {}";

    /**
     * 查询当前产品信息-根据key查询当前产品信息: {}
     */
    String FIND_PRD_INFO_BY_KEY = "查询当前产品信息-根据key查询当前产品信息: {}";

    /**
     * 查询当前产品信息-去数据库查询当前产品信息: {}
     */
    String FIND_PRD_INFO_BY_ID = "查询当前产品信息-根据ID查询产品详情: {}";

    /**
     * 修改当前产品信息-赋值结果: {}
     */
    String ASSIGN_RETURN = "修改当前产品信息-赋值结果: {}";

    /**
     * 修改当前产品信息-修改产品信息结果(1.修改成功 0.修改失败): {}
     */
    String UPDATE_PRD_INFO_RETURN = "修改当前产品信息-修改产品信息结果(1.修改成功 0.修改失败): {}";

    /**
     * 删除当前产品信息-当前用户登录信息: {}
     */
    String USER_LOGIN_INFO = "删除当前产品信息-当前用户登录信息: {}";


    // TODO RoleSecurityServiceImpl
    /**
     * 查询角色权限分页信息: {}
     */
    String FIND_ROLE_PAGE_INFO = "查询角色权限分页信息: {}";

    /**
     * 查询当前所选用户和权限信息-查询角色信息: {}
     */
    String FIND_ROLE_INFO = "查询当前所选用户和权限信息-查询角色信息: {}";

    /**
     * 查询当前所选用户和权限信息-根据角色id查询所拥有的权限id: {}
     */
    String FIND_ROLE_SECURITY_BY_ID = "查询当前所选用户和权限信息-根据角色id查询所拥有的权限id: {}";

    /**
     * 查询当前所选用户和权限信息-根据角色名称查询权限集合: {}
     */
    String FIND_SECURITY_COLLATION_BY_NAME = "查询当前所选用户和权限信息-根据角色名称查询权限集合: {}";

    /**
     * 查询当前所选用户和权限信息-Map集合: {}
     */
    String MAP_COLLATION = "查询当前所选用户和权限信息-Map集合: {}";

    /**
     * 新增用户权限信息-根据权限id查询权限的状态: {}
     */
    String FIND_SECURITY_STATUS_BY_SECURITY_ID = "新增用户权限信息-根据权限id查询权限的状态: {}";

    /**
     * 新增用户权限信息-查询当前角色所拥有的权限ID: {}
     */
    String FIND_ROLE_SECURITY_ID = "查询当前角色所拥有的权限ID: {}";

    /**
     * 查询当前角色权限-根据角色id查询权限id: {}
     */
    String FIND_SECURITY_ID_BY_ROLE_ID = "查询当前角色权限-根据角色id查询权限id: {}";

    /**
     * 修改当前角色权限信息-根据rid查询角色名称: {}
     */
    String FIND_ROLE_NAME_BY_RID = "修改当前角色权限信息-根据rid查询角色名称: {}";

    /**
     * 修改当前角色权限信息-根据rmid查询权限名称: {}
     */
    String FIND_SECURITY_NAME_BY_RMID = "修改当前角色权限信息-根据rmid查询权限名称: {}";

    /**
     * 删除当前角色权限-删除当前角色权限(1.成功  0.失败): {}
     */
    String DEL_ROLE_SECURITY = "删除当前角色权限-删除当前角色权限(1.成功  0.失败): {}";

}
