package com.ncamc.internal;

/**
 * jwt 参数工具类
 */
public interface JwtConstant
{
    String JWT_KEY_ID = "id";
    String JWT_KEY_USER_NAME = "username";

    /**
     * 没有该数据
     */
    String STR_NOT_DATA = "没有该数据";

    /**
     * 解析失败
     */
    String STR_ANALYSIS_ERROR = "解析失败";

    /**
     * 认证失败请从新登录
     */
    String STR_JWT_ERROR = "认证失败请从新登录";

    /**
     * 您的权限不足
     */
    String STR_YOUS_SECURITY_DEFICIENCY = "您的权限不足";

    /**
     * 公钥和私钥生成失败{}
     */
    String STR_PUBKEYPATH_AND_PRIKEYPATH_ERROR = "公钥和私钥生成失败{}";
}
