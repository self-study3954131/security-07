package com.ncamc.internal;

/**
 * @Author : hugaoqiang 2023-07-14
 * 时间常量
 */
public interface DateConstant
{
    String ISO_DATE = "yyyy-MM-dd";
    String ISO_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
}
