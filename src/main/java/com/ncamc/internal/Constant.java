package com.ncamc.internal;

/**
 * 字符串常量
 */
public interface Constant {
    /**
     * 空字符串
     */
    String STR_EMPTY = "";

    /**
     * 数字 零
     */
    String INT_ZERO = "0";

    /**
     * 数字 壹
     */
    String INT_ONE = "1";

    /**
     * 部门
     */
    String DEPT = "部门";

    /**
     * admin
     */
    String admin = "admin";

    /**
     * Admin
     */
    String Admin = "Admin";

    /**
     * 管理员
     */
    String ADMIN = "管理员";

    /**
     * 前缀user:
     */
    String CACHE_KEY_USER = "user:";

    /**
     * header中的token
     */
    String JWT_HEADER_TOKEN = "token";

    /**
     * 查询成功
     */
    String STR_FIND_OK = "查询成功";

    /**
     * 添加成功
     */
    String STR_ADD_OK = "添加成功";

    /**
     * 修改成功
     */
    String STR_UPDATE_OK = "修改成功";

    /**
     * 删除成功
     */
    String STR_DEL_OK = "删除成功";

    /**
     * 退出成功
     */
    String STR_OUT_OK = "退出成功";

    /**
     * 停用成功
     */
    String STR_DEACTIVATE_OK = "停用成功";

    /**
     * 恢复成功
     */
    String STR_RESTORE_OK = "恢复成功";

    /**
     * 修改失败
     */
    String STR_UPDATE_ERROR = "修改失败";

    /**
     * 添加失败
     */
    String STR_ADD_ERROR = "添加失败";

    /**
     * 删除失败
     */
    String STR_DEL_ERROR = "删除失败";


    // TODO ProductServiceImpl
    /**
     * xls
     */
    String xls = "xls";

    /**
     * xlsx
     */
    String xlsx = "xlsx";

    /**
     * XLS
     */
    String XLS = "XLS";

    /**
     * XLSX
     */
    String XLSX = "XLSX";

    /**
     * 产品数据
     */
    String PRD_DATA = "产品数据";

    /**
     * 上传失败
     */
    String UPLOAD_FAILURE = "上传失败";

    /**
     * 记录产品数据中文图表
     */
    String TEXT = "记录产品数据中文图表";

    /**
     * 文件格式不对[只接收xls、xlsx], 上传的格式是：
     */
    String FILE_FORMAT = "文件格式不对[只接收xls、xlsx], 上传的格式是：";


    // TODO RoleSecurityServiceImpl
    /**
     * 请选择权限名称
     */
    String SELECT_SECURITY_NAME = "请选择权限名称";

    /**
     * 请选择角色权限名称
     */
    String SELECT_ROLE_SECURITY_NAME = "请选择角色权限名称";

    /**
     * 修改前和修改后的权限名称一样，请重新修改
     */
    String SECURITY_NAME_ALIKE = "修改前和修改后的权限名称一样，请重新修改";

    /**
     * 该角色没有权限，请先添加权限
     */
    String ROLE_NOT_SECURITY_ADD_SECURITY = "该角色没有权限，请先添加权限";

    /**
     * 权限已经停用，请重新选择权限名称
     */
    String SECURITY_STOP_SELECT_SECURITY_NAME = "权限已经停用，请重新选择权限名称";

    /**
     * 当前角色已经拥有该权限，请重新选择权限名称
     */
    String ROLE_EXIST_SECURITY_SELECT_SECURITY_NAME = "当前角色已经拥有该权限，请重新选择权限名称";


    // TODO RoleServiceImpl
    /**
     * 使用
     */
    String USE = "使用";

    /**
     * 请输入角色名称
     */
    String INPUT_ROLE_NAME = "请输入角色名称";

    /**
     * 角色名称必须全部大写
     */
    String ROLE_NAME_MAST_BIG = "角色名称必须全部大写";

    /**
     * 角色名称相同，请重新修改
     */
    String STR_ROLE_NAME_XT = "角色名称已存在，请重新修改";

    /**
     * 已有角色名称，请重新添加
     */
    String ROLE_NAME_EXIST = "角色名称已存在，请重新添加";

    /**
     * 该角色名称是管理员唯一标识，无法修改
     */
    String ROLE_NAME_IS_ADMIN_NOT_UPDATE = "该角色名称是管理员唯一标识，无法修改";

    /**
     * 该角色有用户在使用，请先解除用户和角色关联
     */
    String STR_ROLE_YOU_NICKNAME_SY = "该角色有用户在使用，请先解除用户和角色关联，被用户：";


    // TODO SecurityServiceImpl
    /**
     * 已有权限标识
     */
    String STR_SECURITY_BS_EXITS = "已有权限标识";

    /**
     * 已有权限名称
     */
    String STR_SECURITY_NAME_EXITS = "已有权限名称";

    /**
     * 请输入权限名称
     */
    String INPUT_SECURITY_NAME = "请输入权限名称";

    /**
     * 权限必须输入三位或者四位
     */
    String SECURITY_MUST_IS_TREE_FOUR = "权限必须输入三位或者四位";

    /**
     * 权限名称是管理员唯一标识，无法修改
     */
    String SECURITY_NAME_IS_ADMIN_NOT_UPDATE = "权限名称是管理员唯一标识，无法修改";

    /**
     * 修改后的权限名称不能和原来一样，请重新修改
     */
    String SECURITY_NAME_NOT_ALIKE = "修改后的权限名称不能和原来一样，请重新修改";

    /**
     * "该权限有角色在使用，请先解除角色和权限关联"
     */
    String STR_SECURITY_YOU_NICKNAME_SY = "该权限有角色在使用，请先解除角色和权限关联，被角色为：";


    // TODO UserRoleServiceImpl
    /**
     * 请选择角色
     */
    String SELECT_ROLE = "请选择角色";

    /**
     * 请选择用户角色
     */
    String SELECT_USER_ROLE = "请选择用户角色";

    /**
     * 角色已经停用，请重新选择角色名称
     */
    String STR_ROLE_STOP = "角色已经停用，请重新选择角色名称";

    /**
     * 角色名称已经存在，请重新选择
     */
    String STR_ROLE_NAME_EXIST = "角色名称已经存在，请重新选择";

    /**
     * 当前用户没有角色，请先添加角色
     */
    String STR_USER_NOT_ROLE = "当前用户没有角色，请先添加角色";

    /**
     * 修改前和修改后的角色相同，请重新修改
     */
    String ROLE_ALIKE = "修改前和修改后的角色相同，请重新修改";

    /**
     * 当前用户已经拥有该角色，请重新选择角色
     */
    String USER_EXIST_ROLE = "当前用户已经拥有该角色，请重新选择角色";


    // TODO UserServiceImpl
    /**
     * 前缀login：
     */
    String CACHE_KEY_LOGIN = "login:";

    /**
     * 登录存储的token
     */
    String LOGIN_TOKEN = "login_token:";

    /**
     * 登录存储的用户
     */
    String LOGIN_USER = "login_user:";

    /**
     * 请输入密码
     */
    String INPUT_PWD = "请输入密码";

    /**
     * 登录成功
     */
    String STR_LOGIN_OK = "登录成功";

    /**
     * 请输入用户名
     */
    String INPUT_NICKNAME = "请输入用户名";

    /**
     * 请输入原密码
     */
    String INPUT_ORIGINAL_PWD = "请输入原密码";

    /**
     * 请输入新密码
     */
    String INPUT_NEW_PWD = "请输入新密码";

    /**
     * 请输入确认密码
     */
    String INPUT_CONFIRM_PWD = "请输入确认密码";

    /**
     * 请输入账号
     */
    String INPUT_USERNAME = "请输入账号";

    /**
     * 账户停用
     */
    String STR_STOP = "该账户已停用，请联系管理员";


    /**
     * 没有该用户请从新登录
     */
    String STR_NOT_USER_NEW_LOGIN = "没有该用户请从新登录";

    /**
     * 账号或者密码错误
     */
    String STR_ACCOUNT_OR_PASSWORD_ERROR = "账号或者密码错误";

    /**
     * 账户不能为admin，请重新输入账号
     */
    String USERNAME_NOT_IS_ADMIN = "账户不能为admin，请重新输入账号";

    /**
     * 用户名不能输入管理员，请重新输入用户名
     */
    String NICKNAME_NOT_INPUT_ADMIN = "用户名不能输入管理员，请重新输入用户名";

    /**
     * 超过登录次数
     */
    String STR_EXCEED_LOGIN_NUMBER = "该账户已停用，限登录五次，请联系管理员";

    /**
     * 原密码不一致
     */
    String STR_OPASSWORD_NOT_CONSISTENT = "原密码不一致";

    /**
     * 新密码不能和原密码一致
     */
    String STR_NEWPASSWORD_NOT_OPASSWORD_CONSISTENT = "新密码不能和原密码一致";

    /**
     * 新密码和确认密码不一致
     */
    String STR_NEWPASSWORD_AND_CONFIRMPASSWORD_NOT_CONSISTENT = "新密码和确认密码不一致";
}
