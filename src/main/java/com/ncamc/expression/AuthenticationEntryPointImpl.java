package com.ncamc.expression;

import com.alibaba.fastjson.JSON;
import com.ncamc.internal.JwtConstant;
import com.ncamc.utils.ServletUtils;
import com.ncamc.entity.dto.ApiResult;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 认证失败处理类
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint
{
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
    {
        ApiResult result = new ApiResult(HttpStatus.UNAUTHORIZED.value(), JwtConstant.STR_JWT_ERROR, null);
        String json = JSON.toJSONString(result);
        ServletUtils.renderString(response, json);
    }
}
