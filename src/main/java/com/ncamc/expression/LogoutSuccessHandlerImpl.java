package com.ncamc.expression;

import com.alibaba.fastjson.JSON;
import com.ncamc.entity.LoginUser;
import com.ncamc.internal.Constant;
import com.ncamc.internal.TokenService;
import com.ncamc.utils.RedisCache;
import com.ncamc.utils.ServletUtils;
import com.ncamc.entity.dto.ApiResult;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义退出处理类
 */
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler
{
    @Resource
    private RedisCache redisCache;

    @Resource
    private TokenService tokenService;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
    {
        String key = tokenService.getRedisKey();
        LoginUser loginUser = tokenService.getLoginUser();
        if (StringUtils.isEmpty(loginUser))
        {
            // 删除用户缓存记录
            redisCache.deleteObject(key);
        }
        ServletUtils.renderString(response, JSON.toJSONString(ApiResult.ok(HttpStatus.OK.value(), Constant.STR_OUT_OK)));
    }
}
