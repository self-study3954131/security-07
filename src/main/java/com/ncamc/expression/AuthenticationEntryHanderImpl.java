package com.ncamc.expression;

import com.alibaba.fastjson.JSON;
import com.ncamc.internal.JwtConstant;
import com.ncamc.utils.ServletUtils;
import com.ncamc.entity.dto.ApiResult;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 权限不足处理类
 */
@Component
public class AuthenticationEntryHanderImpl implements AccessDeniedHandler
{
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
    {
        ApiResult result = new ApiResult<>(HttpStatus.FORBIDDEN.value(), JwtConstant.STR_YOUS_SECURITY_DEFICIENCY, null);
        String json = JSON.toJSONString(result);
        ServletUtils.renderString(response, json);
    }
}
