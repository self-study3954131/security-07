package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Product;
import com.ncamc.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.cursor.Cursor;

import java.util.Map;

public interface ProductMapper extends BaseMapper<Product> {
    /**
     * 多表分页模糊条件查询
     */
    String wrapperSql = "select * from sys_prd p left join sys_factory f on p.factory_id = f.id ${ew.customSqlSegment}";
    String wrapperIdSql = "select * from sys_prd p left join sys_factory f on p.factory_id = f.id where f.id = #{id} ${ew.customSqlSegment}";

    @Select(wrapperSql)
    Page<Map<String, Object>> page(Page<Map<String, Object>> page, @Param("ew") QueryWrapper<Product> productQueryWrapper);

    @Select(wrapperSql)
    Page<Map<String, Object>> pages(Page<Map<String, Object>> page, @Param("ew") QueryWrapper<User> userQueryWrapper);

    @Select(wrapperIdSql)
    Page<Map<String, Object>> pageByIdAndLikeName(Page<Map<String, Object>> page, QueryWrapper<Product> productQueryWrapper, @Param("id") Integer id);

    /**
     * 修改删除操作修改人
     *
     * @param nickName
     * @param id
     */
    @Update("UPDATE sys_user SET update_by = #{nickName} WHERE id = #{id} ")
    void updateBy(String nickName, Long id);

    @Select("select * from sys_prd limit #{limit}")
    Cursor<Map<String, Object>> selectCursor(@Param("limit") int limit);

    @Select("SELECT\n" +
            "(SELECT COUNT(0) FROM sys_prd WHERE DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= DATE(new_date)) '近7天',\n" +
            "\t(SELECT COUNT( 0 ) FROM sys_prd WHERE TO_DAYS( new_date ) = TO_DAYS(NOW())) '当天',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE YEARWEEK(DATE_FORMAT(new_date,'%Y-%m-%d')) = YEARWEEK(NOW())) '本周',\n" +
            "\t(SELECT COUNT(0)FROM sys_prd WHERE DATE_FORMAT(new_date,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m')) '本月',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE QUARTER(new_date) = QUARTER(NOW())) '本季度',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE YEAR(new_date) = YEAR(NOW())) '今年'\n" +
            "FROM\n" +
            "\tsys_prd\n" +
            "GROUP BY\n" +
            "'近7天','当天','本周','本月','本季度','今年';")
    Map<String, Object> selectXY1();

    @Select("SELECT\n" +
            "\t(select COUNT(0) FROM sys_prd WHERE YEAR(new_date) = YEAR(DATE_SUB(NOW(),interval 1 year))) '去年',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE YEARWEEK(DATE_FORMAT(new_date,'%Y-%m-%d')) = YEARWEEK(NOW()) - 1) '上周',\n" +
            "\t(SELECT COUNT(0) from sys_prd WHERE DATE_FORMAT(new_date,'%Y-%m') = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH),'%Y-%m')) '上个月',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE QUARTER(new_date) = QUARTER(DATE_SUB(NOW(),interval 1 QUARTER))) '上个季度',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE TO_DAYS(NOW()) - TO_DAYS(new_date) = 1) '昨天',\n" +
            "\t(SELECT COUNT(0) FROM sys_prd WHERE DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= DATE(new_date)) '近30天'\n" +
            "FROM\n" +
            "\tsys_prd\n" +
            "GROUP BY\n" +
            "'昨天','上周','上个月','上个季度','去年','近30天';")
    Map<String, Object> selectXY2();
}
