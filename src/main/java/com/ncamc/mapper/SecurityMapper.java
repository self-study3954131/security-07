package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SecurityMapper extends BaseMapper<Menu>
{
    /**
     * 查询所有权限数量
     *
     * @return
     */
    Long selectCount();

    /**
     * 根据mid查询角色权限
     *
     * @param id
     * @return
     */
    List<String> selectRoleMenu(@Param("id") Long id);

    /**
     * 根据权限id查询权限名称
     *
     * @param mid
     * @return
     */
    String selectMenuName(@Param("mid") Long mid);
}
