package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleSecurityMapper extends BaseMapper<Menu>
{
    /**
     * 查询角色权限分页信息
     *
     * @param page
     * @param roleName
     * @return
     */
    Page<Map<String, Object>> findAllRoleSecurityPage(@Param("page") Page<User> page, @Param("roleName") String roleName);

    /**
     * 查询当前用户所拥有的角色
     *
     * @param id
     * @return
     */
    String selectUserRoleById(@Param("id") Long id);

    /**
     * 根据角色id查询所拥有的权限id
     *
     * @param id
     * @return
     */
    List<Menu> selectRoleMenuByIds(@Param("id") Long id);

    /**
     * 查询所有权限
     *
     * @return
     */
    List<Menu> getAllMenu();

    /**
     * 根据权限id查询权限的状态
     *
     * @param mid
     * @return
     */
    String findSecurityStatus(@Param("mid") int mid);

    /**
     * 根据角色id查询角色名称
     *
     * @param id
     * @return
     */
    String selectRoleName(@Param("rid") Integer id);

    /**
     * 根据角色id查询权限名称
     *
     * @param rmid
     * @return
     */
    String selectMenuName(@Param("rmid") Integer rmid);

    /**
     * 查询当前角色所拥有的权限
     *
     * @param rid
     * @return
     */
    List<String> selectRoleMenuById(@Param("rid") Long rid);

    /**
     * 往sys_role_menu添加关联
     *
     * @param rid
     * @param mid
     */
    int saveRoleMenu(@Param("rid") Integer rid, @Param("mid") Integer mid);

    /**
     * 修改sys_role_menu表关联
     *
     * @param mid
     * @return
     */
    int updateRoleMenu(@Param("rid") Integer rid, @Param("mid") Integer mid, @Param("rmid") Integer rmid);

    /**
     * 删除当前用户权限
     *
     * @param rid
     * @return
     */
    int delUserSecurityByUID(@Param("rid") int rid, @Param("mid") int mid);
}
