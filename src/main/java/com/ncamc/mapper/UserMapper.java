package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface UserMapper extends BaseMapper<User>
{
    /**
     * 查询所有用户
     *
     * @return
     */
    Long FindAllUser();

    /**
     * 添加登录时间
     *
     * @param map
     */
    int saveLoginTime(@Param("params") Map<String, Object> map);

    /**
     * 修改登录次数
     *
     * @param number
     * @param id
     * @return
     */
    int updateNumberById(@Param("number") Integer number, @Param("id") Long id);

    /**
     * 修改状态
     *
     * @param status
     * @param id
     * @return
     */
    int updateStatusById(@Param("status") String status, @Param("id") Long id);

    /**
     * 获取用户最后登录时间
     *
     * @param username
     * @return
     */
    String getLoginTime(@Param("username") String username);

    /**
     * 修改密码
     *
     * @param password
     * @param id
     * @return
     */
    Integer updatePasswordById(@Param("password") String password, @Param("id") Long id);

    /**
     * 删除用户所拥有的角色
     *
     * @param id
     */
    Integer delUser(@Param("id") Long id);

    /**
     * 修改当前删除操作的修改人
     *
     * @param nickName
     * @param id
     */
    void updateBy(@Param("nickName") String nickName, @Param("id") Long id);
}
