package com.ncamc.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface UserRoleSecurityMapper
{
    /**
     * 查询用户角色权限分页信息
     *
     * @param page
     * @param nickName
     * @return
     */
    Page<Map<String, Object>> findAllUserRoleSecurityPage(@Param("page") Page<Menu> page, @Param("nickName") String nickName, @Param("username") String username);
}
