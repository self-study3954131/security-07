package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Role;
import com.ncamc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserRoleMapper extends BaseMapper<Role>
{
    /**
     * 查询所有用户角色分页信息
     *
     * @param page
     * @param nickName
     * @param username
     * @return
     */
    Page<Map<String, Object>> findAllUserRolePage(@Param("page") Page<User> page, @Param("nickName") String nickName, String username);

    /**
     * 查询当前用户所拥有的角色
     *
     * @param id
     * @return
     */
    List<Role> selectUserRole(@Param("id") Long id);

    /**
     * 查询所有角色信息
     *
     * @return
     */
    List<Role> findAllRole();

    /**
     * 根据角色id查询角色的状态
     *
     * @param rid
     * @return
     */
    String findRoleStatus(@Param("rid") int rid);

    /**
     * 根据用户名查询roleID
     *
     * @param nickName
     * @return
     */
    List<String> findRoleID(@Param("nickName") String nickName);

    /**
     * 往sys_user_role添加关联
     *
     * @param uid
     * @param rid
     */
    void saveUserRole(@Param("uid") Long uid, @Param("rid") Integer rid);

    /**
     * 查询当前用户所拥有的角色
     *
     * @param id
     * @return
     */
    Integer selectUserRoleById(@Param("id") Long id);

    /**
     * 根据用户id查询用户名称
     *
     * @param uid
     * @return
     */
    String selectUserName(@Param("uid") int uid);

    /**
     * 根据用户角色id查询角色名称
     *
     * @param urid
     * @return
     */
    String selectRoleName(@Param("urid") int urid);

    /**
     * 查询当前用户所拥有的角色
     *
     * @param uid
     * @return
     */
    List<String> selectUserRoleByIds(@Param("uid") int uid);

    /**
     * 修改sys_user_role关联
     *
     * @param uid
     * @param rid
     * @param urid
     */
    int updateUserRole(@Param("uid") Integer uid, @Param("rid") Integer rid, @Param("urid") int urid);

    /**
     * 删除当前用户权限
     *
     * @param uid
     * @param rid
     * @return
     */
    int delUserRole(@Param("uid") int uid, @Param("rid") int rid);
}
