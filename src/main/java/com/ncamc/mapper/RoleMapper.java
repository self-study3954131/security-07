package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role>
{
    /**
     * 查询所有角色数量
     *
     * @return
     */
    Long selectCount();

    /**
     * 查询用户角色信息
     *
     * @param id
     * @return
     */
    List<String> selectUserRole(@Param("id") Long id);
}
