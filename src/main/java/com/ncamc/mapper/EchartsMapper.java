package com.ncamc.mapper;

import org.apache.ibatis.annotations.Select;

import java.util.Map;

public interface EchartsMapper
{
    @Select("SELECT * FROM ecs_sy_charts")
    Map<String, Integer> charts();

    @Select("SELECT * FROM ecs_sy_components")
    Map<String, Integer> components();

    @Select("SELECT * FROM ecs_sy_downloadJson")
    Map<String, Integer> downloadJson();

    @Select("SELECT * FROM ecs_sy_themeJson")
    Map<String, Integer> themeJson();

}
