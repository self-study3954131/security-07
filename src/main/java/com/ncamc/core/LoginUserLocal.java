package com.ncamc.core;

import com.ncamc.entity.User;

/**
 * @Author : hugaoqiang 2023-07-13
 */
public class LoginUserLocal
{
    private static final ThreadLocal<User> threadLocal = new ThreadLocal<>();

    public static void set(User user)
    {
        threadLocal.set(user);
    }

    public static void remove()
    {
        threadLocal.remove();
    }

    public static User get()
    {
        return threadLocal.get();
    }

}
