package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.User;
import com.ncamc.service.UserService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api("用户user接口")
@RestController
@RequestMapping("/user")
public class UserController
{
    @Resource
    private UserService userService;

    @ApiOperation("查询用户分页信息")
    @PostMapping("/findAllUserPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "username", value = "用户名", dataTypeClass = String.class, example = "admin")
    })
    public ApiResult list(@RequestBody Map<String, Object> params)
    {
        Page<User> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return userService.listPage(page, params);
    }

    @ApiOperation("新增用户信息")
    @PostMapping("/addUser")
    @PreAuthorize("hasAuthority('system:dept:list')")
    public ApiResult addUser(@RequestBody User user)
    {
        return userService.register(user);
    }

    @ApiOperation("查询当前用户信息")
    @GetMapping("/findByUserId")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult select(Long id)
    {
        return userService.selectById(id);
    }

    @ApiOperation("修改当前用户信息")
    @PostMapping("/updateUser")
    @PreAuthorize("hasAuthority('system:dept:list')")
    public ApiResult update(@RequestBody User user)
    {
        return userService.updateUser(user);
    }

    @ApiOperation("删除当前用户信息")
    @GetMapping("/delUser")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult delUser(Long id)
    {
        return userService.delUser(id);
    }

}
