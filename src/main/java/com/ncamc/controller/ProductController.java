package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.core.GeneratorCommunicateFile;
import com.ncamc.entity.Product;
import com.ncamc.entity.dto.ApiResult;
import com.ncamc.internal.Constant;
import com.ncamc.mapper.ProductMapper;
import com.ncamc.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-07-08 09:44
 */
@Slf4j
@Api("产品product接口")
@RestController
@RequestMapping("/product")
public class ProductController
{
    @Resource
    private ProductService productService;

    @Resource
    private ProductMapper productMapper;

    @ApiOperation("多表分页模糊条件查询")
    @PostMapping("/lists")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "username", value = "用户名", dataTypeClass = String.class, example = "admin"),
            @ApiImplicitParam(name = "id", value = "产品ID", dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult getProductList(@RequestBody Map<String, Object> params)
    {
        Page<Map<String, Object>> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return productService.getProductList(page, params);
    }

    @ApiOperation("查询产品分页信息")
    @PostMapping("/findAllProductPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "prdIns", value = "产品/机构名称", dataTypeClass = String.class, example = "产品test1/机构test1")
    })
    public ApiResult list(@RequestBody Map<String, Object> params)
    {
        Page<Product> page = new Page<>(MapUtils.getIntValue(params, "pageNo", 0), MapUtils.getIntValue(params, "pageSize"));
        return productService.listPage(page, params);
    }

    @ApiOperation("新增产品信息")
    @PostMapping("/addProduct")
    public ApiResult add(@RequestBody Product product)
    {
        return productService.add(product);
    }

    @ApiOperation("查询当前产品信息")
    @GetMapping("/findProductById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产品ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult select(Long id)
    {
        return productService.findById(id);
    }

    @ApiOperation("修改当前产品信息")
    @PostMapping("/updateProduct")
    public ApiResult update(@RequestBody Product product)
    {
        return productService.updateProduct(product);
    }

    @ApiOperation("删除当前产品信息")
    @GetMapping("/delProduct")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产品ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult del(Long id)
    {
        return productService.deleteByPrimaryKey(id);
    }

    @ApiOperation("柱状图")
    @GetMapping("/getDatas1")
    public ApiResult getDatas()
    {
        return ApiResult.ok(Constant.STR_EMPTY, productService.getDatas());
    }

    @ApiOperation("饼图")
    @GetMapping("/getDatas2")
    public ApiResult getDatas2()
    {
        return ApiResult.ok(Constant.STR_EMPTY, productService.getDatas2());
    }

    @ApiOperation("导出产品信息")
    @GetMapping("/exprot")
    public void exprot(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String fileName = String.format("Product_%s.xlsx", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        List<Product> list = productService.list();
        if (CollectionUtils.isEmpty(list))
        {
            log.error("product data is null: {}", list);
        }

        byte[] reviewBytes = GeneratorCommunicateFile.generatorProductProspectRecordFile(list);
//        WebUtils.out(request, response, reviewBytes, fileName, Boolean.TRUE);
    }

    @ApiOperation("导入Excel")
    @PostMapping("/upExcel")
    public ApiResult upExcel(@RequestParam("file") MultipartFile file)
    {
        productService.upExcel(file);
        return ApiResult.ok(HttpStatus.OK.value(), "上传成功");
    }

    @ApiOperation("修改对应WPS文件")
    @GetMapping("/wps")
    public String wps() throws IOException
    {
//        // 通过流加载WPS文字文档
//        FileInputStream inputStream = new FileInputStream("F:/L/东京奥运会.wps");
//        Document document = new Document();
//        document.loadFromStream(inputStream, FileFormat.Doc);
//
//        // 查找所有“北京冬奥会”文本
//        TextSelection[] textSelections = document.findAllString("国", false, false);
//        // 设置文本高亮色、加粗
//        for (TextSelection selection : textSelections)
//        {
//            selection.getAsOneRange().getCharacterFormat().setHighlightColor(Color.YELLOW);
//            selection.getAsOneRange().getCharacterFormat().setBold(true);
//        }
//
//        // 获取文档的第一个节
//        Section section = document.getSections().get(0);
//
//        // 获取第2段，设置段落背景色
//        Paragraph paragraph1 = section.getParagraphs().get(1);
//        paragraph1.getFormat().setBackColor(new Color(176, 224, 230));
//        paragraph1.getStyle().getParagraphFormat().setHorizontalAlignment(com.spire.doc.documents.HorizontalAlignment.Center);
//
//        // 获取第3段，添加图片到段落
//        Paragraph paragraph2 = section.getParagraphs().get(2);
//        DocPicture picture = paragraph2.appendPicture("F:/PNG/shouye1.png");
//        picture.setWidth(200f);
//        picture.setHeight(250f);
//        picture.setTextWrappingStyle(TextWrappingStyle.Through);
//
//
//        // 将结果文档保存到流
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        document.saveToStream(bos, FileFormat.Doc);
//        // 将流写入WPS文档
//        FileOutputStream fos = new FileOutputStream("Output.wps");
//        fos.write(bos.toByteArray());
//        // 关闭流
//        bos.close();
//        fos.close();
        return "success";
    }
}
