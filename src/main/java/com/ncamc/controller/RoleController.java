package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Role;
import com.ncamc.service.RoleService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api("角色role接口")
@RestController
@RequestMapping("/role")
public class RoleController
{
    @Resource
    private RoleService roleService;

    @ApiOperation("查询角色分页信息")
    @PostMapping("/findAllRolePage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "roleName", value = "角色名称", required = true, dataTypeClass = String.class, example = "CEO")
    })
    public ApiResult findAllPageRole(@RequestBody Map<String, Object> params)
    {
        Page<Role> page = new Page<>(MapUtils.getLongValue(params, "pageNo"), MapUtils.getLongValue(params, "pageSize"));
        return roleService.findAllPageRole(page, params);
    }

    @ApiOperation("新增角色信息")
    @PostMapping("/saveRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleName", value = "角色名称", required = true, dataTypeClass = String.class, example = "CEO"),
            @ApiImplicitParam(name = "roleKey", value = "角色权限标识", required = true, dataTypeClass = String.class, example = "ceo")
    })
    public ApiResult saveRole(@RequestBody Map<String, Object> params)
    {
        return roleService.saveRole(params);
    }

    @ApiOperation("查询角色是否被使用")
    @GetMapping("/selectRoleUserById")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult selectRoleUserById(Long id)
    {
        return roleService.selectRoleUserById(id);
    }

    @ApiOperation("查询当前角色信息")
    @GetMapping("/findRoleByID")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult findRoleByID(Long id)
    {
        return roleService.findRoleByID(id);
    }

    @ApiOperation("修改当前角色信息")
    @PostMapping("/updateRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色id", required = true, dataTypeClass = String.class, example = "1"),
            @ApiImplicitParam(name = "roleName", value = "角色名称", required = true, dataTypeClass = String.class, example = "CEO")
    })
    public ApiResult updateRole(@RequestBody Map<String, Object> params)
    {
        return roleService.updateRole(params);
    }

    @ApiOperation("删除当前角色信息")
    @GetMapping("/delRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult delRole(Long id)
    {
        return roleService.delRole(id);
    }

    @ApiOperation("停用/恢复权限")
    @GetMapping("/deactivate")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult deactivate(Long id)
    {
        return roleService.deactivate(id);
    }

}
