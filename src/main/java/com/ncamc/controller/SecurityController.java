package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.service.SecurityService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api("权限security接口")
@RestController
@RequestMapping("/security")
public class SecurityController
{
    @Resource
    private SecurityService securityService;

    @ApiOperation("查询权限分页信息")
    @PostMapping("/findAllSecurityPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "menuName", value = "权限名称", required = true, dataTypeClass = String.class, example = "测试")
    })
    public ApiResult list(@RequestBody Map<String, Object> params)
    {
        Page<Menu> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return securityService.list(page, params);
    }

    @ApiOperation("新增权限标识")
    @PostMapping("/addSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuName", value = "权限名称", required = true, dataTypeClass = String.class, example = "测试"),
            @ApiImplicitParam(name = "perms", value = "权限标识", required = true, dataTypeClass = String.class, example = "test")
    })
    public ApiResult add(@RequestBody Map<String, Object> params)
    {
        return securityService.add(params);
    }

    @ApiOperation("查询权限是否被使用")
    @GetMapping("/selectSecurityRoleId")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult selectSecurityRoleId(Long id)
    {
        return securityService.selectSecurityRoleId(id);
    }

    @ApiOperation("查询当前权限信息")
    @GetMapping("/findSecurityById")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult findById(Long id)
    {
        return securityService.findById(id);
    }

    @ApiOperation("修改当前权限信息")
    @PostMapping("/updateSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1"),
            @ApiImplicitParam(name = "menuName", value = "权限名称", required = true, dataTypeClass = String.class, example = "测试")
    })
    public ApiResult update(@RequestBody Map<String, Object> params)
    {
        return securityService.update(params);
    }

    @ApiOperation("删除当前权限信息")
    @GetMapping("/delSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult del(Long id)
    {
        return securityService.del(id);
    }

    @ApiOperation("停用/恢复权限")
    @GetMapping("/deactivate")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "权限ID", required = true, dataTypeClass = Long.class, example = "1")
    )
    public ApiResult deactivate(Long id)
    {
        return securityService.deactivate(id);
    }

}
