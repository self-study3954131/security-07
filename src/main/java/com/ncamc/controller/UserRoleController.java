package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.User;
import com.ncamc.service.UserRoleService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@Api("用户角色userRole接口")
@RestController
@RequestMapping("/userRole")
public class UserRoleController
{
    @Resource
    private UserRoleService userRoleService;

    @ApiOperation("查询所有用户角色分页信息")
    @PostMapping("/findAllUserRolePage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "nickName", value = "用户名", dataTypeClass = String.class, example = "admin")
    })
    public ApiResult findAllUserRolePage(@RequestBody Map<String, Object> params)
    {
        Page<User> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return userRoleService.findAllUserRolePage(page, params);
    }

    @ApiOperation("查询所有角色信息")
    @GetMapping("/findAllRole")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1")
    )
    public ApiResult findAllRole(Long id)
    {
        return userRoleService.findAllRole(id);
    }

    @ApiOperation("添加用户角色")
    @PostMapping("/saveUserRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "角色名称", required = true, dataTypeClass = List.class, example = "CEO"),
            @ApiImplicitParam(name = "mid", value = "权限ID", required = true, dataTypeClass = List.class, example = "1")
    })
    public ApiResult saveUserRole(@RequestBody Map<String, Object> params)
    {
        return userRoleService.saveUserRole(params);
    }

    @ApiOperation("查询当前用户是否拥有角色")
    @GetMapping("/selectUserRoleById")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1")
    )
    public ApiResult selectUserRoleByIdU(Long id)
    {
        return userRoleService.selectUserRoleByIdU(id);
    }

    @ApiOperation("修改当前角色信息")
    @PostMapping("/updateUserRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "用户名称", required = true, dataTypeClass = String.class, example = "admin"),
            @ApiImplicitParam(name = "rid", value = "角色ID", required = true, dataTypeClass = String.class, example = "1")
    })
    public ApiResult updateUserRole(@RequestBody Map<String, Object> params)
    {
        return userRoleService.updateUserRole(params);
    }

    @ApiOperation("删除当前用户角色")
    @PostMapping("/delUserRole")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "rid", value = "角色ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult delUserRole(@RequestBody Map<String, Object> params)
    {
        return userRoleService.delUserRole(params);
    }

}
