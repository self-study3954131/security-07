package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.Menu;
import com.ncamc.service.UserRoleSecurityService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api("用户角色权限userRoleSecurity接口")
@RestController
@RequestMapping("/userRoleSecurity")
public class UserRoleSecurityController
{
    @Resource
    private UserRoleSecurityService userRoleSecurityService;

    @ApiOperation("查询用户角色权限分页信息")
    @PostMapping("/findAllUserRoleSecurityPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "menuName", value = "权限名称", required = true, dataTypeClass = String.class, example = "测试")
    })
    public ApiResult findAllUserRoleSecurityPage(@RequestBody Map<String, Object> params)
    {
        Page<Menu> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return userRoleSecurityService.findAllUserRoleSecurityPage(page, params);
    }

}
