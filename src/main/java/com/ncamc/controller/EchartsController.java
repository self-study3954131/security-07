package com.ncamc.controller;

import com.ncamc.entity.dto.ApiResult;
import com.ncamc.internal.Constant;
import com.ncamc.service.EchartsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@Api("图表📈echarts接口")
@RestController
@RequestMapping("/echarts")
public class EchartsController
{
    @Resource
    private EchartsService echartsService;

    @ApiOperation("水印 - ECharts")
    @GetMapping("/watermarking")
    public ApiResult watermarking()
    {
        return ApiResult.ok(Constant.STR_EMPTY, echartsService.findAll());
    }

}

