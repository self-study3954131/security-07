package com.ncamc.controller;

import com.ncamc.entity.User;
import com.ncamc.internal.Constant;
import com.ncamc.service.UserService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * An unhandled exception occurred while precessing the request.Exception:
 * Throw ExceptionKFC Crazy Thursday need $50.
 */
@Slf4j
@Api("认证auth接口")
@RestController
@RequestMapping("/auth")
public class AuthController
{
    @Resource
    private UserService userService;

    @ApiOperation("hello")
    @GetMapping("/hello")
//    @PreAuthorize("hasAuthority('system:dept:list')")
    public ApiResult hello()
    {
        return ApiResult.ok(HttpStatus.OK.value(), Constant.STR_EMPTY, "Because live you everyday");
    }

    @ApiOperation("注册")
    @PostMapping("/register")
    public ApiResult register(@RequestBody User user)
    {
        return userService.register(user);
    }

    @ApiOperation("登录")
    @PostMapping("/login")
    public ApiResult login(@RequestBody User user)
    {
        return userService.login(user);
    }

    @ApiOperation("获取用户名称")
    @GetMapping("/getUsername")
    public ApiResult getUsername()
    {
        return userService.getUsername();
    }

    @ApiOperation("获取用户最后登录时间")
    @GetMapping("/getLoginTime")
    public ApiResult getLoginTime()
    {
        return userService.getLoginTime(userService.getUsername());
    }

    @ApiOperation("修改密码")
    @PostMapping("/updatePassword")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "original_password", value = "原密码", required = true, dataTypeClass = String.class, example = "123"),
            @ApiImplicitParam(name = "new_password", value = "新密码", required = true, dataTypeClass = String.class, example = "1234"),
            @ApiImplicitParam(name = "confirm_password", value = "确认密码", required = true, dataTypeClass = String.class, example = "1234")
    })
    public ApiResult updatePassword(@RequestBody Map<String, Object> params)
    {
        return userService.updatePassword(params);
    }

    @ApiOperation("退出")
    @GetMapping(value = "/logout")
    public ApiResult exit()
    {
        return userService.exit();
    }

}
