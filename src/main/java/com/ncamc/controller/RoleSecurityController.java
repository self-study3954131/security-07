package com.ncamc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.User;
import com.ncamc.service.RoleSecurityService;
import com.ncamc.entity.dto.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@Api("角色权限RoleSecurity接口")
@RestController
@RequestMapping("/roleSecurity")
public class RoleSecurityController
{
    @Resource
    private RoleSecurityService roleSecurityService;

    @ApiOperation("查询角色权限分页信息")
    @PostMapping("/findAllRoleSecurityPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "当前页条数", required = true, dataTypeClass = Integer.class, example = "5"),
            @ApiImplicitParam(name = "roleName", value = "角色名称", dataTypeClass = String.class, example = "admin")
    })
    public ApiResult findAllRoleSecurityPage(@RequestBody Map<String, Object> params)
    {
        Page<User> page = new Page<>(MapUtils.getIntValue(params, "pageNo"), MapUtils.getIntValue(params, "pageSize"));
        return roleSecurityService.findAllRoleSecurityPage(page, params);
    }

    @ApiOperation("查询当前所选角色和权限信息")
    @GetMapping("/findRoleSecurityByID")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Integer.class, example = "1")
    )
    public ApiResult findRoleSecurityByID(Long id)
    {
        return roleSecurityService.findRoleSecurityByID(id);
    }

    @ApiOperation("新增角色权限信息")
    @PostMapping("/saveRoleSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "角色名称", required = true, dataTypeClass = List.class, example = "CEO"),
            @ApiImplicitParam(name = "mid", value = "权限ID", required = true, dataTypeClass = List.class, example = "1")
    })
    public ApiResult saveRoleSecurity(@RequestBody Map<String, Object> params)
    {
        return roleSecurityService.saveRoleSecurity(params);
    }

    @ApiOperation("查询当前角色权限")
    @GetMapping("/selectRoleMenuById")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Integer.class, example = "1")
    )
    public ApiResult selectRoleMenuById(Long id)
    {
        return roleSecurityService.selectRoleMenuById(id);
    }

    @ApiOperation("修改当前角色权限信息")
    @PostMapping("/updateRoleSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "用户名", required = true, dataTypeClass = String.class, example = "admin"),
            @ApiImplicitParam(name = "mid", value = "权限ID", required = true, dataTypeClass = String.class, example = "1")
    })
    public ApiResult updateRoleSecurity(@RequestBody Map<String, Object> params)
    {
        return roleSecurityService.updateRoleSecurity(params);
    }

    @ApiOperation("删除当前角色权限")
    @PostMapping("/delRoleSecurity")
    @PreAuthorize("hasAuthority('system:dept:list')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Integer.class, example = "1"),
            @ApiImplicitParam(name = "mid", value = "权限ID", required = true, dataTypeClass = Integer.class, example = "1")
    })
    public ApiResult delRoleSecurity(@RequestBody Map<String, Object> params)
    {
        return roleSecurityService.delRoleSecurity(params);
    }

}
