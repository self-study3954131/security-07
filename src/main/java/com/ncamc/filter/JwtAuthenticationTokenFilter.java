package com.ncamc.filter;

import com.ncamc.internal.NotNull;
import com.ncamc.config.JwtProperties;
import com.ncamc.entity.LoginUser;
import com.ncamc.internal.Constant;
import com.ncamc.internal.JwtConstant;
import com.ncamc.utils.JwtUtils;
import com.ncamc.utils.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * token过滤器 验证token有效性
 */
@Slf4j
@Configuration
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter
{
    @Resource
    private RedisCache redisCache;

    @Resource
    private JwtProperties jwtProperties;

    @Override
    protected void doFilterInternal(HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain) throws ServletException, IOException
    {
        String requestURI = request.getRequestURI();
        // 过滤静态资源
        if (requestURI.contains("admin") || requestURI.contains("plugins") || requestURI.contains("css") || requestURI.contains("img"))
        {
            filterChain.doFilter(request, response);
            return;
        }
        log.info("请求路径: {}", requestURI);
        // 获取token
        String token = request.getHeader(Constant.JWT_HEADER_TOKEN);
        log.info("token: {}", token);
        if (!StringUtils.hasText(token))
        {
            filterChain.doFilter(request, response);
            return;
        }
        // 解析token
        Long uid;
        try
        {
            uid = JwtUtils.getInfoFromId(token, jwtProperties.getPublicKey());
            log.info("uid: {}", uid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(JwtConstant.STR_ANALYSIS_ERROR);
        }
        // 从redis中获取用户
        String key = Constant.LOGIN_USER + uid;
        LoginUser loginUser = redisCache.getCacheObject(key);
        log.info("loginUser: {}", loginUser);
        if (Objects.isNull(loginUser))
        {
            throw new RuntimeException(Constant.STR_NOT_USER_NEW_LOGIN);
        }
        // 存入SecurityContextHolder
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(request, response);
    }
}
