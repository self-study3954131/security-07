package com.ncamc.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("API返回参数")
public class ApiResult<T>
{
    @ApiModelProperty(
        value = "响应消息",
        required = false
    )
    private String message;
    @ApiModelProperty(
        value = "响应码",
        required = true
    )
    private Integer code;
    @ApiModelProperty(
        value = "响应数据",
        required = false
    )
    private T data;

    public static ApiResult fail(String message) {
        return new ApiResult(ResultCode.BAD_REQUEST.getCode(), message, (Object)null);
    }

    public static ApiResult fail(Integer code, String message) {
        return new ApiResult(code, message, (Object)null);
    }

    public static ApiResult notFound() {
        return new ApiResult(ResultCode.NOT_FOUND.getCode(), ResultCode.NOT_FOUND.getDesc(), (Object)null);
    }

    public static ApiResult notFound(String message) {
        return new ApiResult(ResultCode.NOT_FOUND.getCode(), message, (Object)null);
    }

    public static ApiResult forbidden() {
        return new ApiResult(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getDesc(), (Object)null);
    }

    public static ApiResult forbidden(String message) {
        return new ApiResult(ResultCode.FORBIDDEN.getCode(), message, (Object)null);
    }

    public static ApiResult unauthorized(String message) {
        return new ApiResult(ResultCode.UNAUTHORIZED.getCode(), message, (Object)null);
    }

    public static ApiResult unauthorized() {
        return new ApiResult(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getDesc(), (Object)null);
    }

    public static ApiResult internalError() {
        return new ApiResult(ResultCode.INTERNAL_SERVER_ERROR.getCode(), ResultCode.INTERNAL_SERVER_ERROR.getDesc(), (Object)null);
    }

    public static ApiResult internalError(String message) {
        return new ApiResult(ResultCode.INTERNAL_SERVER_ERROR.getCode(), message, (Object)null);
    }

    public static ApiResult ok() {
        return new ApiResult(ResultCode.SUCCESS.getCode(), "OK", (Object)null);
    }

    public static ApiResult ok(String message) {
        return new ApiResult(ResultCode.SUCCESS.getCode(), message, (Object)null);
    }

    public static ApiResult ok(Integer code, String message) {
        return new ApiResult(code, message);
    }

    public static ApiResult ok(String message, Object data) {
        return new ApiResult(ResultCode.SUCCESS.getCode(), message, data);
    }

    public static ApiResult ok(Integer code, String message, Object data) {
        return new ApiResult(code, message, data);
    }

    public static ApiResult lockByOther(Object data) {
        return new ApiResult(ResultCode.LOCK_BY_OTHER.getCode(), ResultCode.LOCK_BY_OTHER.getDesc(), data);
    }

    public static ApiResult performFailed() {
        return new ApiResult(ResultCode.PERFORM_FAILED.getCode(), ResultCode.PERFORM_FAILED.getDesc());
    }

    public static ApiResult unlock() {
        return new ApiResult(ResultCode.UNLOCK.getCode(), ResultCode.UNLOCK.getDesc());
    }

    public static ApiResult build(Integer code, String message, Object data) {
        return new ApiResult(ResultCode.SUCCESS.getCode(), message, data);
    }

    public static ApiResult build(Integer code, String message) {
        return new ApiResult(code, message, (Object)null);
    }

    public ApiResult() {
    }

    public ApiResult(Integer code, String msg, T data) {
        this.code = code;
        this.message = msg;
        this.data = data;
    }

    public ApiResult(String message) {
        this(ResultCode.SUCCESS.getCode(), message, null);
    }

    public ApiResult(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public ApiResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiResult(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getDesc();
    }

    public ApiResult(T data) {
        this.code = ResultCode.SUCCESS.getCode();
        this.message = ResultCode.SUCCESS.getDesc();
        this.data = data;
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getCode() {
        return this.code;
    }

    public T getData() {
        return this.data;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public void setData(final T data) {
        this.data = data;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ApiResult)) {
            return false;
        } else {
            ApiResult<?> other = (ApiResult)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$code = this.getCode();
                    Object other$code = other.getCode();
                    if (this$code == null) {
                        if (other$code == null) {
                            break label47;
                        }
                    } else if (this$code.equals(other$code)) {
                        break label47;
                    }

                    return false;
                }

                Object this$message = this.getMessage();
                Object other$message = other.getMessage();
                if (this$message == null) {
                    if (other$message != null) {
                        return false;
                    }
                } else if (!this$message.equals(other$message)) {
                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ApiResult;
    }

    public int hashCode() {
        int result = 1;
        Object $code = this.getCode();
        result = result * 59 + ($code == null ? 43 : $code.hashCode());
        Object $message = this.getMessage();
        result = result * 59 + ($message == null ? 43 : $message.hashCode());
        Object $data = this.getData();
        result = result * 59 + ($data == null ? 43 : $data.hashCode());
        return result;
    }

    public String toString() {
        return "ApiResult(message=" + this.getMessage() + ", code=" + this.getCode() + ", data=" + this.getData() + ")";
    }
}
