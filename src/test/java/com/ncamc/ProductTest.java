package com.ncamc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ncamc.entity.EchartsBean;
import com.ncamc.entity.Product;
import com.ncamc.internal.Constant;
import com.ncamc.internal.DateConstant;
import com.ncamc.mapper.ProductMapper;
import com.ncamc.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cursor.Cursor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@SpringBootTest
public class ProductTest {

    @Resource
    private ProductService productService;

    @Resource
    private ProductMapper productMapper;

    @Test
    public void test01() {
        Product product = new Product();
        int count = productService.count();
        for (int i = 1; i <= 424809; i++) {
            product.setId(count + i);
            product.setPrdName("产品test" + (count + i));
            product.setPrdDm("1");
            product.setNet("0.8812");
            product.setFbalance("123.1");
            product.setFavalable("123.1");
            product.setInsName("机构test" + (count + i));
            product.setInsDm("001");
            if (i <= 330000) {
                product.setNewDate("2022-05-10");
                product.setCreateTime("2022-05-10 00:00:00");
            } else if (i <= 660000) {
                product.setNewDate("2022-06-13");
                product.setCreateTime("2022-06-13 00:00:00");
            } else {
                product.setNewDate("2-22-07-16");
                product.setCreateTime("2-22-07-16 00:00:00");
            }
            product.setCreateBy(Constant.ADMIN);
            productService.save(product);
        }
    }

    @Test
    public void update() {
        List<Product> list = productService.list();

        for (int i = 0; i < list.size(); i++) {
            Product product = list.get(i);

            product.setId(i);
            product.setPrdName("产品test" + i);
            product.setInsName("机构test" + i);
            product.setUpdateBy(Constant.ADMIN);
            product.setUpdateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateConstant.ISO_DATE_TIME)));

            productService.update(product,new LambdaQueryWrapper<Product>().eq(Product::getId,product.getId()));
        }
    }

    @Test
    @Transactional
    public void cursor()throws Exception{
        int limit = 10;
        //会报错 A Cursor is already closed.（游标已关闭）
        try (Cursor<Map<String,Object>> cursor = productMapper.selectCursor(limit)) {
            System.out.println("index:" + cursor.getCurrentIndex());
            System.out.println("=======================");
            System.out.println("cursor:" + cursor);
            System.out.println("=======================");
            System.out.println();
            cursor.forEach(product -> {
                System.out.println("product:" + product);
            });
        }

//        try (Cursor<Map<String,Object>> cursor = productMapper.selectCursors()) {
//            cursor.forEach(product -> {});
//        }

//        List<Product> products = productMapper.selectList(null);
//        System.out.println(products);
    }

    @Test
    public void test(){
        EchartsBean echartsBean = barChartData();
        System.out.println(echartsBean.getxAxisCategory());
        System.out.println(echartsBean.getDatas());
    }

    /**
     * 柱形图数据拼装
     * @return
     */
    @Test
    public EchartsBean barChartData() {
        Map<String, Object> map = productMapper.selectXY1();
        EchartsBean echartsBean = new EchartsBean();

        Set<Map.Entry<String, Object>> entries = map.entrySet();
        List<String> listX = new ArrayList<>();
        List<Integer> listY = new ArrayList<>();
        for (Map.Entry<String, Object> entry : entries) {
            listX.add(entry.getKey());
            listY.add(((Number) entry.getValue()).intValue());
        }
        echartsBean.setxAxisCategory(listX);
        echartsBean.setDatas(listY);
        return echartsBean;
    }

    /**
     * 饼图数据拼装
     * @return
     */
    @Test
    public void barChartData1() {
        Map<String, Object> map = productMapper.selectXY1();

        Map<String,Object> mapXY = new HashMap<>();
        Map<String,Object> maps = new HashMap<>();
        List<String> data = new ArrayList<>();

        Set<Map.Entry<String, Object>> entries = map.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            mapXY.put("name",entry.getKey());
            mapXY.put("value",entry.getValue());
            data.add(JSONObject.toJSONString(mapXY));
        }

        maps.put("data",JSON.parse(data.toString()));
        maps.put("trigger","产品数据中文图表");
        maps.put("name","产品数据");

        System.out.println(maps);
    }
}
