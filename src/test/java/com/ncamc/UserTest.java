package com.ncamc;

import com.ncamc.config.JwtProperties;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import com.ncamc.utils.CollectionUtils;
import com.ncamc.utils.JwtUtils;
import com.ncamc.utils.Md5Utile;
import io.lettuce.core.cluster.SlotHash;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-07-05 10:09
 */
@Slf4j
@SpringBootTest
public class UserTest {

    @Resource
    private UserMapper userMapper;

    @Resource
    private JwtProperties jwtProperties;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Test
    public void Demo(){
        String encode = passwordEncoder.encode("admin@1234");
        System.out.println(encode);
    }

    @Test
    public void test1() {
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }

    /**
     * MD5加密&&解密
     */
    @Test
    public void test2(){
        boolean matches = passwordEncoder.matches("123456", "$2a$10$AV9Xz.3ck4RsXiad5ArcBO5.ZKwlpcnJzs740BHY..fsSp0PnM/Zu");
//        boolean matches = passwordEncoder.matches("123", "$2a$10$RgwDRQQEOyT.uzqmlI9ZzefAlJp0I5UsSd89p3chxXd3p57yZt19K");
        System.out.println(matches);
        System.out.println("============");
        String salt = Md5Utile.generateSalt();
        String md5Hex = Md5Utile.md5Hex("123", salt);
        log.info("salt: {}", salt);
        log.info("md5HeX: {}", md5Hex);
        String shaHex = Md5Utile.md5Hex("123", salt);
        log.info("shaHeX: {}", shaHex);
    }

    /**
     * 生成token&&解析token
     */
    @Test
    public void test3() {
        String token = JwtUtils.generateToken("1", jwtProperties.getPrivateKey(), jwtProperties.getExpire());
        Long id = JwtUtils.getInfoFromId(token, jwtProperties.getPublicKey());
        System.out.println(id);
    }

    @Test
    public void test4() {
        System.out.println(SlotHash.getSlot("A"));
        System.out.println(SlotHash.getSlot("B"));
        System.out.println(SlotHash.getSlot("C"));
        System.out.println(SlotHash.getSlot("hello"));
    }

    @Test
    public void test5() {
        List<String> list1 = new ArrayList<>();
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("4");
        list1.add("9");
        List<String> list2 = new ArrayList<>();
        list2.add("3");
        list2.add("4");
        list2.add("5");
        list2.add("6");

        Collection diff1 = CollectionUtils.diff(list1, list2);// list1中有，list2中没有的元素
        System.out.println(diff1.toString());
        Collection diff2 = CollectionUtils.diff(list2, list1);// list1中没有，list2中有的元素
        System.out.println(diff2.toString());
    }
}
