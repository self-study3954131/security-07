### 添加需求：
#### 产品列表添加导入✅、模版。✅
#### 首页添加产品数据（100w条数据、加索引）和柱状图✅&饼图✅。
#### 添加logback.xml日志。✅
#### 全局logs打印（访问全url✅、访问时间✅、响应结果✅）。
#### 使用sh脚本部署jar包（流放）
https://blog.csdn.net/qq_41307110/article/details/125228428?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522167843938116800180657714%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=167843938116800180657714&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-2-125228428-null-null.142^v73^insert_down2,201^v4^add_ask,239^v2^insert_chatgpt&utm_term=sh%E8%84%9A%E6%9C%AC%E9%83%A8%E7%BD%B2springboot%E9%A1%B9%E7%9B%AE&spm=1018.2226.3001.4187
#### 将http请求修改为https（流放）
#### 使用docker部署jar包（流放）

### bug
页码显示存在一定问题，但不影响功能

### 架构: SpringBoot+security+mybatisPlus
### jdk: 1.8
### 技术: lombok、swagger2、Dockerfile、redis、mysql5.7、jwt、RabbitMQ、Echarts
